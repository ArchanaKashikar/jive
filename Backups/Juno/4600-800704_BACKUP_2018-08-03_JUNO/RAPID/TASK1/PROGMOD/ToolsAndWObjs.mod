MODULE ToolsAndWObjs
!!!----------------------------------********---------------------------------------------!!!
! Company Confidential
! ArevoGroovy ToolsandWobj Version 1.0
! Generated at Arevo | 8/3/2018 | Authors - Archana Kashikar
! Purpose - This module tool data including tool definition as per ABB's tooldata datatype format
!           and other critical tool data such as dimensions of some key features
!           This also contains work object data as per ABB's wobjdata datatype
!
! Changes-
!!!----------------------------------********---------------------------------------------!!!
    
!!!!---------------Robot Parameters-------------------!!!!
    PERS string BPMechUnitName := "M7DM1";
    
    
!!!!------------------Tool Definitions-----------------!!!!


!Tool definition of Cylindrical calibration tool. 
TASK PERS tooldata tool_RenishawCylinder:=   [TRUE,[[0,0,285.22],[1,0,0,0]],[0.001,[0,0,0.001],[1,0,0,0],0,0,0]];
TASK PERS num RenishawCylinderMachinedDia := 25.4;
TASK PERS num RenishawCylinderMachinedLen := 55;

!Tool definition of ATI tool master that comprises of back adapter, extended black adapter and master side. 
TASK PERS tooldata tool_Master:=   [TRUE,[[0,0,78.0],[1,0,0,0]],[0.001,[0,0,0.001],[1,0,0,0],0,0,0]];
    
!Tool definition of Extruder 2.0. 
PERS tooldata tool_LaserEndEffector:=[TRUE,[[-0.146593,-0.36159,299.263],[1,0,0,0]],[7,[108.79,16,102.52],[1,0,0,0],0,0,0]];!298.163

!Tool definition of Distance laser. 
PERS tooldata tool_DistLaser:=[TRUE,[[43.86,0,203.295],[0,0,0,1]],[0.001,[0,0,0.001],[1,0,0,0],0,0,0]];

!Tool definition of Touch Probe 
PERS tooldata tool_TouchProbe:=[TRUE,[[-0.211534,0.739009,287.899],[1,0,0,0]],[0.001,[0,0,0.001],[1,0,0,0],0,0,0]];

!Tool definition of Renishaw Touch Probe 
PERS tooldata tool_RenishawProbe:=[TRUE,[[-0.280208,-2.6716,324.448],[1,0,0,0]],[0.001,[0,0,0.001],[1,0,0,0],0,0,0]];
PERS num RenishawProbeRubySphereDia := 6;

PERS tooldata tool_Scan0 := [TRUE,[[-11.35,108.89,345.63],[0,0,0,1]],[0.001,[0,0,0.001],[1,0,0,0],0,0,0]];
!PERS tooldata toolScan1 := [TRUE,[[-84.30,-181.46,315.98],[0,0,0,1]],[0.001,[0,0,0.001],[1,0,0,0],0,0,0]];
PERS tooldata tool_Scan1 := [TRUE,[[-286.695,0,-17.427],[0,-0.70711,0,0.70711]],[0.001,[0,0,0.001],[1,0,0,0],0,0,0]];

PERS tooldata tool_Pointer := [TRUE,[[0,0,312.5],[0,0,0,1]],[0.001,[0,0,0.001],[1,0,0,0],0,0,0]];

!Tool definition of milling tool
PERS tooldata tool_Milling:=[TRUE,[[-76.526,-0.065072,292.788],[0.930462,-3.81813E-05,-0.366388,-1.5055E-05]],[7,[108.79,16,102.52],[1,0,0,0],0,0,0]];

!!!!------------------WObj Definitions-----------------!!!!

!Copy of rotating build plate Work object to work with
PERS wobjdata wobj_staticBP:=[FALSE, TRUE,"",[[1396.55, 13.1912, 382.904],[0.999986,0.00427985,-0.00303505,1.34368E-05]],[[0,0,0],[1,0,0,0]]];

PERS wobjdata wobj_renishawToolSetter170Ruby:=[FALSE, TRUE,"",[[1717.97,-19.2141,652.477],[1,0,0,0]],[[0,0,0],[1,0,0,0]]];
!Copy of rotating build plate Work object to work with
PERS wobjdata wobj_rotBP:=[FALSE, FALSE,"M7DM1",[[0,0,0],[1,0,0,0]],[[0,0,0],[1,0,0,0]]];

PERS wobjdata wobj_MasterRing := [FALSE, TRUE,"",[[1407.41,1.38502,421.571],[0.999987,0.00477033,-0.00190138,-4.58554E-05]],[[-5.43,27.438,0],[1,0,0,0]]];
PERS num MasterSphereDia := 19.05;

ENDMODULE