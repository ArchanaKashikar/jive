MODULE PointCloud
!!!----------------------------------********---------------------------------------------!!!
! Company Confidential
! ArevoGroovy PointCloud Version 1.0
! Generated at Arevo | 8/3/2018 | Author - Archana Kashikar, Wiener Mondesir
! Purpose - This module contains procedures to generate pointcloud using scan tools available 
!           in Arevo printers and save the point cloud to hard disk of the robot.
!
! Changes-
!!!----------------------------------********---------------------------------------------!!!

    RECORD pointdata
        pos point;
        num data;
    ENDRECORD
        
        ALIAS num scanToolType;
        CONST scanToolType Banner := 1;
        CONST scanToolType Gocator := 2;
        
        
        
        !!!****Define these points using your tool of interest on Teach Pendant****!!!
        CONST robtarget pt1:=[[-273.17,-78.47,47.64],[0.000227721,-0.00421409,-0.999988,-0.00234273],[0,-3,0,1],[9E+09,0.00109667,0.000107338,9E+09,9E+09,9E+09]];
    	CONST robtarget pt2:=[[291.40,94.83,47.64],[0.000226659,-0.00421051,-0.999988,-0.00234077],[0,-3,0,1],[9E+09,0.00109667,8.46413E-06,9E+09,9E+09,9E+09]];
        
    	PERS tooldata scanTool := [TRUE,[[-11.35 ,108.89,345.63],[0,0,0,1]],[0.001,[0,0,0.001],[1,0,0,0],0,0,0]];
        
        LOCAL VAR pos min;
        LOCAL VAR pos max;
        
        PERS num N;
        PERS pointdata points{1500};
        PERS scanToolType scantoolName;
        PERS string pointCloudFileName := "PointCloud_RH.ply";
        
    
     PROC PointCloud_Main()
        
        VAR pointdata points{1500}; ! Number of Cloud Points
        VAR robtarget p0;
        VAR jointtarget target;
        VAR num scanwidth := 28; !mm
        
        !!!********Name of the Scanning Tool*****!!!
        scantoolName := Banner; 
        
        
        min := pt1.trans;
        max := pt2.trans;
        
        N := Dim(points,1);
        p0 := pt1;
        
        !Generate Point Cloud Positions
        IF scantoolName = Banner THEN !Banner
            scanTool := tool_Scan0;
            p0.robconf := [-1,-1,-1,1];
            GoArmHome;
            GoBPHome;
            CreateScanPoints min, max, points; 
        ELSEIF scantoolName = Gocator THEN !Gocator
            scanTool := tool_Scan1;
            GoArmHome;
            GoGocatorHome;
            p0.robconf := [-1,-2,-1,0];
            CreateScanLines min, max, scanwidth, points;
        ENDIF
            
        ConfL\On;
        SingArea\Wrist;
        
        target := CJointT();
        p0.extax := target.extax;
        
        RRI_Open;
        
        FOR i FROM 1 TO N DO
            
            ! Move Robot to Point Cloud Positions
            p0.trans := points{i}.point;
            
            IF p0.trans.z < pt1.trans.z THEN
                IF scantoolName = Banner THEN !Banner
                    GoBPHome;
                    SavePointCloud pointCloudFileName, points;
                ELSEIF scantoolName = Gocator THEN
                    GoGocatorHome;
                    GoArmHome;
                ENDIF
                EXIT;
            ENDIF
            MoveL p0, v100, fine, scanTool\WObj:=wobj_rotBP;
            
            IF scantoolName = Banner THEN !Banner
                WaitTime 0.8;
                points{i}.data := 100 + AInput(aiDistanceLaser)*100/32767; !Store sesnor value as point cloud data
                points{i}.point.z := (164+p0.trans.z) - points{i}.data; ! Or translate z by sensor value !164mm is the distance when banner tool reads 0 in z
            ENDIF
        ENDFOR

        IF scantoolName = Banner THEN !Banner
            GoBPHome;
            SavePointCloud pointCloudFileName, points;
        ELSEIF scantoolName = Gocator THEN
            GoGocatorHome;
            GoArmHome;
        ENDIF
        
        ConfL\Off;
        IF scantoolName = Banner THEN !Banner
            !Save data to file default is HOME: directory
            SavePointCloud pointCloudFileName, points;
        ENDIF
        
        RRI_Close;
    ENDPROC
    
    PROC CreateScanPoints( pos min, pos max, INOUT pointdata points{*} )
        VAR num rows;
        VAR num cols;
        VAR pos point;
        VAR num i;
        VAR num x_delta;
        VAR num y_delta;
        
        i := 1;
        !N := Dim(points,1);
        rows := 15;!( sqrt(N) ); !along y
        cols := 100;!( sqrt(N) ); !along x
        N := rows*cols;
        x_delta := (max.x - min.x) / (cols);
        y_delta := (max.y - min.y) / (rows);
        
        
        FOR row FROM 0 TO rows DO
            FOR col FROM 0 TO cols DO
                IF i <= N THEN
                    points{i}.point := [min.x + col*x_delta, min.y + row*y_delta, (min.z + max.z) / 2];
                    !IF col MOD 2 = 1  points{i}.point.y := min.y + (rows-row)*y_delta;               
                    IF row MOD 2 = 1  points{i}.point.x := min.x + (cols-col)*x_delta;               
                    
                    !Initial value
                    points{i}.data := 0;
                    i := i + 1;
                ENDIF
            ENDFOR
        ENDFOR
    ENDPROC
    
     PROC CreateScanLines( pos min, pos max,  num width, INOUT pointdata points{*})
        VAR num rows;
        VAR num cols;
        VAR pos point;
        VAR num i;
        VAR num x_delta;
        VAR num y_delta;
        
        i := 1;
        
        x_delta := (max.x - min.x);
        y_delta := width;
        
        cols := 2;
        rows := Trunc((max.y - min.y)/y_delta) + 1;
        
        N := cols * rows;
        
        
        FOR row FROM 0 TO rows DO
            FOR col FROM 0 TO cols DO
                IF i <= N THEN
                    points{i}.point := [min.x + col*x_delta, min.y + row*y_delta, (min.z + max.z) / 2];
                    !IF col MOD 2 = 1  points{i}.point.y := min.y + (rows-row)*y_delta;               
                    IF row MOD 2 = 1  points{i}.point.x := min.x + (cols-col)*x_delta;               
                    
                    !Initial value
                    points{i}.data := 0;
                    i := i + 1;
                    
                ENDIF
            ENDFOR
        ENDFOR
    ENDPROC
    
    PROC SavePointCloud( string filename, pointdata points{*} )
        
        VAR num N;
        VAR iodev ioFile;
        VAR pos point;
        
        N := Dim(points,1);
        
        !Reset File
        IF IsFile(filename) RemoveFile filename;
        Open filename,ioFile\Write;
        
        !Create Header
        Write ioFile, "ply";
        Write ioFile, "format ascii 1.0";
        Write ioFile, "comment Arevo generated";
        Write ioFile, "element vertex " + ValToStr(N);
        Write ioFile, "property float x";
        Write ioFile, "property float y";
        Write ioFile, "property float z";
        Write ioFile, "property float intensity";
        Write ioFile, "end_header";
        
        !Save Points
        FOR i FROM 1 TO N DO
            point := points{i}.point;
            Write ioFile, NumToStr(point.x,5)+" "+NumToStr(point.y,5)+" "+NumToStr(point.z,5)+" "+NumToStr(points{i}.data,5);
        ENDFOR
        
        Close ioFile;
    ENDPROC
    
    PROC Path_Test()
    
        VAR speeddata sp := V500;
        VAR robtarget p0;
        VAR num dip := -5;
        VAR num dip_length := 2;
        p0 := CRobT();
        
        MoveL Offs(p0, 100,0,0),  sp, z0, tool_LaserEndEffector, \Wobj:=wobj_staticBP;
        MoveL Offs(p0, 100,0,dip), sp, z0, tool_LaserEndEffector, \Wobj:=wobj_staticBP;
        MoveL Offs(p0, 100 + dip_length,0,dip),  sp, z0, tool_LaserEndEffector, \Wobj:=wobj_staticBP;
        MoveL Offs(p0, 100 + dip_length,0,0),  sp, z0, tool_LaserEndEffector, \Wobj:=wobj_staticBP;
        MoveL Offs(p0, 200,0,0),  sp, z0, tool_LaserEndEffector, \Wobj:=wobj_staticBP;
        MoveL Offs(p0, 100 + dip_length,0,0),  sp, z0, tool_LaserEndEffector, \Wobj:=wobj_staticBP;
        MoveL Offs(p0, 100 + dip_length,0,dip),  sp, z0, tool_LaserEndEffector, \Wobj:=wobj_staticBP;
        MoveL Offs(p0, 100,0,dip), sp, z0, tool_LaserEndEffector, \Wobj:=wobj_staticBP; 
        MoveL Offs(p0, 100,0,0),  sp, z0, tool_LaserEndEffector, \Wobj:=wobj_staticBP;
        MoveL p0,  sp, z0, tool_LaserEndEffector, \Wobj:=wobj_staticBP;
        
	    
	ENDPROC
    
ENDMODULE