MODULE ToolDefs_DED
!!!----------------------------------********---------------------------------------------!!!
! Company Confidential
! ArevoGroovy ToolDefs_DED Version 1.0
! Generated at Arevo | 8/3/2018 | Author - Archana Kashikar, Wiener Mondesir
! Purpose - This module contains AM functions that maneuver the robot and perform 
!           process specific IO communication with peripheral components in the Arevo printers
!           according to the RAPID command sequence generated from typically a gcode.
!           ToolDefs_DED perform robot manuevers for DED(Direct Energy Deposition) process
!
! Changes-
!!!----------------------------------********---------------------------------------------!!!




!-----------Print Time Stat Variables--------! 
RECORD printTimes
    num stripnum;
    num touchdownTime;
    num backoffTime;
    num approachTime;
    num extensionTime;
    num liftupTime;
    num buildplateTime;
    num travelTime; 
ENDRECORD

PERS printTimes stripTimes := [1,0.19,0.281,0.768,0.199,0.181,1.497,1.536];
PERS bool touchdown := FALSE; !Flag used to identify touchdown
PERS bool liftup := FALSE; !Flag used to identify liftup

VAR num touchdownTime:= 0;
VAR num backoffTime:= 0;
VAR num approachTime:= 0;
VAR num extensionTime:= 0;
VAR num liftupTime:= 0;
VAR num buildplateTime:= 0;
VAR num travelTime;
VAR num strip := 0;

VAR clock printClock;
VAR intnum intClockStart;
VAR intnum intClockStop; 
!----End of Print Time Stat Variables--------!


!-----------Robot Homing Positions-----------!
PERS jointtarget JArmHome:=[[0,15,0,0,75,0],[0,9E+09,9E+09,9E+09,9E+09,9E+09]];
CONST jointtarget JBuildPlateHome:=[[0,15,0,0,75,0],[0,9E+09,9E+09,9E+09,9E+09,9E+09]];
PERS jointtarget JGocatorHome:=[[0,15,0,0,15,0],[0,9E+09,9E+09,9E+09,9E+09,9E+09]];
!----End of Robot Homing Positions-----------!

PERS wobjdata currWobj := [FALSE, FALSE,"M7DM1",[[0,0,0],[1,0,0,0]],[[0,0,0],[1,0,0,0]]];
PERS num xOffset := 0;
PERS num yOffset := 0;
PERS num zOffset := 3.7;
PERS num extjOffset := 0;

!-------------anchoring only between start and Fibercut------------!
PERS bool anchor := TRUE;


!--------------Declaration of tools and workobjects objects---------!
LOCAL PERS wobjdata activeWobj:=[FALSE, FALSE,"M7DM1",[[0, 0, 0],[1,0,0,0]],[[0,0,0],[1,0,0,0]]];
LOCAL PERS tooldata activeTool := [TRUE,[[-0.280208,-2.6716,324.448],[1,0,0,0]],[0.001,[0,0,0.001],[1,0,0,0],0,0,0]];
LOCAL PERS tooldata activePrintTool := [TRUE,[[-0.280208,-2.6716,324.448],[1,0,0,0]],[0.001,[0,0,0.001],[1,0,0,0],0,0,0]];
LOCAL PERS tooldata activeScanTool := [TRUE,[[-286.695,0,-17.427],[0,-0.70711,0,0.70711]],[0.001,[0,0,0.001],[1,0,0,0],0,0,0]];

!Obselete
!Tool definition of current tool. 
!PERS tooldata tTool0:=[TRUE,[[-2.50136,-0.160782,296.274],[0,0,0,1]],[15.8,[115.6,-16.6,118.2],[1,0,0,0],0.47,0,0.319]];


!Work Object definition 
!PERS wobjdata wWobj0:=[FALSE, TRUE,"",[[1380.48, -15.8397, 393.314],[0.999998,-0.00202798,-0.000732422,0]],[[0,0,0],[1,0,0,0]]];

!PERS wobjdata wWobj1:=[FALSE,FALSE,"M7DM1",[[0,0,0],[1,0,0,0]],[[0,0,0],[1,0,0,0]]];
!End of Obselete

!---------End of Declaration of tools and workobjects objects---------!



!----------------Restart data and function defs---------------------!

PERS tooldata toolLast:=[TRUE,[[-2.50136,-0.160782,296.274],[0,0,0,1]],[15.8,[115.6,-16.6,118.2],[1,0,0,0],0.47,0,0.319]];
PERS wobjdata wobjLast:=[FALSE, FALSE,"M7DM1",[[0, 0, 0],[1,0,0,0]],[[0,0,0],[1,0,0,0]]];
PERS robtarget stopPos;

! Target position and speed used by PLC Agent for motion control
PERS robtarget targetPos;

!To capture previous point
PERS robtarget prevPos;
!-----------End of Restart data and function defs---------------------!

!----------Process Parameters-------------!

PERS bool ZLC := TRUE; !True when using zero length cutter
PERS bool toolNormal := TRUE; !For chainstay, toolNormal = FALSE
PERS bool wobjNormal := TRUE; !For chainstay, wobjNormal = FALSE
PERS bool horizontal := TRUE; !For chainstay. horizontal = FALSE;
PERS bool anchorSlowDown := TRUE;
PERS num ZLCAngle := 45.855; !degrees
PERS num ZLCRetract := 42; !mm
PERS num ZLCRetractWaitTime := 0.5; !seconds
PERS num approach := 20; !mm
PERS num toolTiltAngle := 14; !degrees
PERS num ironDist := 10; !mm
PERS num extension := 3; !mm
PERS num backoffSpeedFactor := 10;
PERS num zlcRetractSpeedFactor := 10;
PERS num zlcRetractSpeedInput := 150;
PERS num gcodezOffset := -0.537435; !mm
PERS num reducedAcc := 1.0; !percentage
PERS num accel := 0.004; !mm/sec2
PERS num anchorDistance := 5;
PERS num anchorSpeedFactor := 0.3; !below 1
PERS pos gocatorExposurePos := [300,300,10];
PERS pos gocatorAlignmentPos := [500,300,10];
!---------End of Process Parameters--------!

!---------Robot Parameters-----------------!
PERS confdata PrintConfData := [-1,-1,-1,0];
PERS confdata scanConfData := [-1,-2,-1,0];

PERS num ExtjIndex1 := 1;
PERS num ExtjIndex2 := 2;
PERS num indAxisnum := 1;
!---------End of Robot Parameters----------!

!----------Wobj Oframe components-----------!
PERS num Eux;
PERS num Euy;
PERS num Euz;
PERS num Tx;
PERS num Ty;
PERS num Tz;
!----End of Wobj Oframe components----------!

!------Error Handling-----------------------!
PERS errnum ErrorNum;
VAR num axisnum := 1;
VAR bool cutAcktimeout;
VAR bool startAcktimeout;
VAR bool stopAcktimeout;
VAR errnum ERR_CUTACK:=-1;
VAR errnum ERR_STARTACK:=-1;
VAR errnum ERR_STOPACK:=-1;
VAR errnum ERR_INVALIDZ:=-1;
VAR num cutAckErrorid := 4800;
VAR num startAckErrorid := 4801;
VAR num stopAckErrorid := 4802;
VAR num invalidZErrorid := 4803;
VAR errstr cutAckErrTitle := "FibercutAck Signal Status";
VAR errstr startAckErrTitle := "StartAck Signal Status";
VAR errstr stopAckErrTitle := "StopAck Signal Status";
VAR errstr invalidZErrTitle := "StopAck Signal Status";
VAR errstr cutAckstr := "Fibercut Ack Not Recieved";
VAR errstr startAckstr := "Start Ack Not Recieved";
VAR errstr stopAckstr := "Stop Ack Not Recieved";
VAR errstr invalidZstr := "Invalid Z coordinate";
!------End of Error handling----------------!

!!==================================FUNCTIONS================================!!

FUNC robtarget Lerp(robtarget firstPos, robtarget secondPos, num lerpDistance, \switch first, \switch second)
    VAR num dist;
    VAR robtarget wayPos; 
    
    dist := Distance(firstPos.trans, secondPos.trans);
    
    IF NOT dist = 0 THEN
        IF PRESENT(first) THEN
            IF lerpDistance < dist THEN
                wayPos := secondPos;
                wayPos.trans := (1-(lerpDistance/dist))*firstPos.trans + (lerpDistance/dist)*secondpos.trans;
                RETURN wayPos;
            ELSE
                RETURN secondPos;
            ENDIF
        ELSEIF PRESENT(second) THEN
            IF lerpDistance < dist THEN
                wayPos := secondPos;
                wayPos.trans := (lerpDistance/dist)*firstPos.trans + (1-(lerpDistance/dist))*secondpos.trans;
                RETURN wayPos;
            ELSE
                RETURN firstPos;
            ENDIF
        ELSE
            RETURN secondPos;
        ENDIF
    ELSE 
        RETURN secondPos;
    ENDIF
    
ENDFUNC

!!======================END OF FUNCTIONS=====================================!!


PROC AM_ModifyToolWobj()
    VAR pos adjust;
    Tx := 160.63544 + 0;
    Ty := -6.127 +0;
    Tz := 27.5924 + 3 -1 - 0.1;
    Eux := 0.1718868 + 0;
    Euy := -0.646 + 0.85;
    Euz := 176.54164;
    

    IF (toolNormal = TRUE) THEN
        activePrintTool:= tool_LaserEndEffector;
    ELSE
        activePrintTool:= tool_LaserEndEffector;
        activePrintTool.tframe.rot := OrientZYX(180, toolTiltAngle, 0);
        activePrintTool.tframe.trans := PoseVect(activePrintTool.tframe,[  11 * Sin(toolTiltAngle),0,0.3 + 11 * (1 - Cos(toolTiltAngle))  ]);
    ENDIF
	
	 IF (wobjNormal = TRUE) THEN
            Tx := 0;
            Ty := 0;
            Tz := 0;
            Eux := 0;
            Euy := 0;
            Euz := 0;
        activeWobj := wobj_rotBP;
    ELSE
        activeWobj := wobj_rotBP;
        ! applying mold registration tranformation
        activeWobj.oframe.rot := OrientZYX(Euz,Euy,Eux);
        activeWobj.oframe.trans:=[Tx,Ty,Tz];
        
        ! Adjustments
        activeWobj.oframe.trans.x := activeWobj.oframe.trans.x + 0;
        activeWobj.oframe.trans.y := activeWobj.oframe.trans.y + 0;
        activeWobj.oframe.trans.z := activeWobj.oframe.trans.z + 0;
    ENDIF
ENDPROC
 


! Activate and save the stopped position
PROC AM_StopPos()
    extjOffset := -Euz;
    RRI_Close;
    IF (stopPos.trans.x = 0 AND stopPos.trans.y = 0 AND stopPos.trans.z = 0) THEN 
        stopPos := CRobT(\Tool:=toolLast,\WObj:=wobjLast);
    ENDIF    
ENDPROC

! Reset line count and restart mode to zero
PROC AM_InitRestartData()
    stopPos.trans.x := 0;
    stopPos.trans.y := 0;
    stopPos.trans.z := 0;    
ENDPROC
! End of restart data and function defs

! Start of PLC Agent motion control commands. You must activate the M7DM1 unit from the Pendant first.
! Procedure to move robot robot to target position
PROC AM_SetTargetPos() !may not be needed
    TPWrite("AM_SetTargetPos");
    MoveL targetPos, v20, fine, tool0\WObj:=wobj0;
ENDPROC

! Procedure to get the current target position
PROC AM_GetTargetPos() !may not be needed
    TPWrite("AM_GetTargetPos");
    targetPos := CRobT(\Tool:=tool0,\WObj:=wobj0);
ENDPROC
! End of PLC Agent motion control commands

PROC GoArmHome()
    VAR jointtarget current;
    current := CJointT();
    JArmHome.extax := current.extax;
    MoveAbsJ JArmHome, v100, fine, tool0\WObj:=wobj0;
    JArmHome.extax := [0,9E+09,9E+09,9E+09,9E+09,9E+09];
ENDPROC

PROC GoBPHome()
    MoveAbsJ JBuildPlateHome, v100, fine, tool0\WObj:=wobj0;
ENDPROC 

PROC GoGocatorHome()
    VAR jointtarget current;
    current := CJointT();
    JGocatorHome.extax := current.extax;
    MoveAbsJ JGocatorHome, v100, fine, tool0\WObj:=wobj0;
    JGocatorHome.extax := [0,9E+09,9E+09,9E+09,9E+09,9E+09];
ENDPROC 



!Initializing the time data to zero
PROC stripTimeZero()
    stripTimes := [0,0,0,0,0,0,0,0];
    liftup := FALSE;
    touchdown := FALSE;
    anchor := FALSE;
ENDPROC
PROC AM_ResetExtAxis(extjoint initExtj)
    VAR num firstExtj;
    firstExtj := initExtj.eax_a;
    extjOffset := Trunc(firstExtj/360) * 360;
    extjOffset := extjOffset - Euz;
    IF firstExtj < 0 THEN
        indReset M7DM1, indAxisnum \RefNum:=0, \Fwd;
    ELSE
        indReset M7DM1, indAxisnum \RefNum:=0, \Bwd;
    ENDIF
    
    ERROR
        ErrorNum := ERRNO;
ENDPROC
PROC AM_GocatorExposure()
    GoBPHome;
    GoGocatorHome;
    IF gocatorAlignmentPos.z  < 10 THEN
    BookErrNo ERR_INVALIDZ;
    ErrRaise "ERR_INVALIDZ", invalidZErrorid, invalidZErrTitle, ERRSTR_TASK, invalidZstr, ERRSTR_CONTEXT,ERRSTR_EMPTY;
    EXIT;
    ENDIF
    
    activeScanTool := tool_Scan1;
    activeWobj := wobj_rotBP;
    
    ! Procedure here
    MoveL [gocatorExposurePos, [0,0,-1,0],[-1,-2,-1,0],[9E9,0,0,9E9,9E9,9E9]],v100,fine,activeScanTool\wobj:=activeWobj; 
    WaitTime 5; 
    
    ERROR
    IF ERRNO = ERR_INVALIDZ THEN
        ErrorNum := ERRNO;
        ErrLog invalidZErrorid, invalidZstr,"","","","";
    ENDIF
ENDPROC

PROC AM_GocatorAlignment()
    GoBPHome;
    GoGocatorHome;
    IF gocatorAlignmentPos.z  < 10 THEN
    BookErrNo ERR_INVALIDZ;
    ErrRaise "ERR_INVALIDZ", invalidZErrorid, invalidZErrTitle, ERRSTR_TASK, invalidZstr, ERRSTR_CONTEXT,ERRSTR_EMPTY;
    EXIT;
    ENDIF
    
    activeScanTool := tool_Scan1;
    activeWobj := wobj_rotBP;
    
    ! Procedure here
    MoveL [gocatorAlignmentPos, [0,0,-1,0],[-1,-2,-1,0],[9E9,0,0,9E9,9E9,9E9]],v100,fine,activeScanTool\wobj:=activeWobj; 
    WaitTime 5; 
    
    ERROR
    IF ERRNO = ERR_INVALIDZ THEN
        ErrorNum := ERRNO;
        ErrLog invalidZErrorid, invalidZstr,"","","","";
    ENDIF
    
ENDPROC

!Setting up the robot for printing
PROC AM_Initialize(\switch jobResume, \switch scanJob, pos minPos, pos maxPos, extjoint initExtj)
    
    stripTimeZero;                              !Intialize all time stats to 0
    
    AM_ZeroDO;                                  !Initlialize all digital outputs to 0
    
    GoArmHome;                                  !Home the robot arm
    
    AM_ModifyToolWobj;                          !Assign tool and workobject
    
    RsMaster:=[1,4,3];                          !Close RRI if its open
    IF RsMaster.State <> STATE_CLOSED THEN
        RRI_Close;
    ENDIF
    
    AM_ResetExtAxis(initExtj);                  !Zero set independent axis of buildplate
    
    IF Present(scanJob) THEN
        GoBPHome;
        GoGocatorHome;
        !MoveL Offs([[0,0,(maxPos.z+100)], [0,0,-1,0],[-1,-2,-1,0],[9E9,0,0,9E9,9E9,9E9]],0,0,100),v100,fine,toolScan1\wobj:=wWobj1;
    ELSE
        GoBPHome;
        !MoveL Offs([[0,0,(maxPos.z+100)], [0,0,-1,0],[-1,-1,-1,1],[9E9,0,0,9E9,9E9,9E9]],0,0,100),v100,fine,tTool0\wobj:=wWobj1;
    ENDIF
    

    
    RRI_Open;                                   !Open RRI
    
    ERROR
    ErrorNum := ERRNO;
    
ENDPROC

!Final wrap up after printing
PROC AM_Finalize(\switch scanJob)
    
    VAR robtarget lastpoint;
    
    AM_ZeroDO;                                  !Set all digital outputs to 0 
    
    AM_CaptureTime;                             !Capture all time stats for last strip
    
    RRI_Close;                                  !Close RRI
    
    lastpoint := CRobT(\Tool:=tool0,\WObj:=wobj0);!Lift robot up after the job completion
    MoveL Offs(lastpoint,0,0,100),v50,fine,tool0\wobj:=wobj0;
    
    IndReset M7DM1,2 \RefNum:=0, \Short;         !Reset Independent axis
    extjOffset := - Euz;                        !Reset to required mold alignment
    
    IF Present(scanJob) THEN
        GoGocatorHome;
    ELSE
        GoBPHome;
    ENDIF
    
    GoArmHome;                                  !Home the robot arm
    
    ERROR
    ErrorNum := ERRNO;
ENDPROC

!Do not remove AM_ZeroDO, this procedure is called by the PLC Agent 
PROC AM_ZeroDO()
    SetDO doStart, 0;
    SetDO doStop, 0;
    SetDO doFiberCut, 0;
    SetDO doPause, 0;
    SetDo doIron, 0; 
ENDPROC

!Handler for Stop turn off deposition signals
PROC AM_StopHandler()
    extjOffset := -Euz;
    RRI_Close;
    AM_ZeroDO;
ENDPROC

TRAP trapClockStart
    ClkReset printClock;
    ClkStart printClock;
ENDTRAP

TRAP trapClockStop
    ClkStop printClock;
ENDTRAP

!Bruno has first external axis assigned to eax_c an second external axis asigned to eax_b. To be used only by Bruno
FUNC extjoint AM_AssignExtAxis(extjoint inExt)
    
    VAR extjoint outExt;
    outExt.eax_a := 9E9;
    outExt.eax_b := 0;
    outExt.eax_c := inExt.eax_a - extjOffset;
    outExt.eax_d := 9E9;
    outExt.eax_e := 9E9;
    outExt.eax_f := 9E9;
    RETURN outExt;
ENDFUNC

!Main Deposition routine
PROC AM_MoveL(\switch START | switch STOP | switch FIBERCUT | switch PREPRIME , num nFlow, robtarget pos, speeddata speed, zonedata zone, PERS tooldata tool, \PERS wobjdata wobj)
    
    VAR num epsilon := 0.01;
    VAR bool timeout;
    VAR bool compensate := TRUE;
    VAR bool extend := TRUE;
    VAR robtarget currPos;
    VAR robtarget oriPos;
	VAR robtarget prepos;
    VAR robtarget waypoint1;
    VAR robtarget waypoint2;
    VAR bool margin;
    VAR speeddata backoffSpeed;
    VAR speeddata zlcRetractSpeed;
    VAR speeddata anchorSpeed;
    
    
    VAR triggdata triggFiberCut;
    VAR triggdata triggStart;
    VAR triggdata triggStop;
    VAR triggdata triggIron;
    VAR triggdata triggClockStop;
    VAR triggdata triggClockStart;
    
    VAR triggdata flow;
    VAR triggdata flowrate;
    VAR triggdata flowrateClone;
    VAR triggData TCPSpeed;

    VAR robtarget pausePos;
    VAR robtarget startPos;
    VAR robtarget toPos;
    VAR robtarget fromPos;
    VAR num scale := 1;
    
    IF Present(wobj) THEN
        currWobj := wobj;
    ELSE
        currWobj := wobj0;
    ENDIF
        
    backoffSpeed := speed;
    zlcRetractSpeed := speed;
    backoffSpeed.v_tcp := backoffSpeedFactor*speed.v_tcp;
    !zlcRetractSpeed.v_tcp := zlcRetractSpeedFactor*speed.v_tcp;
    zlcRetractSpeed.v_tcp := zlcRetractSpeedInput;
    
    ! Offset the robot position by the user specified offset
    pos := Offs(pos, xOffset, yOffset, zOffset);
    
    !To be used only for Bruno
    !pos.extax := AM_AssignExtAxis(pos.extax);
    
    !Offset all BP angles by initial extjOffset
    pos.extax.eax_a := pos.extax.eax_a - extjOffset;
    
    ! Trigger declaration
    TriggIO triggFiberCut, 0\Start, \DOp:=doFiberCut, 0;
    TriggIO triggStart, 0\Start, \DOp:=doStart, 0;
    TriggIO triggStop, 0\Start, \DOp:=doStop, 0;
    TriggIO triggIron, 0\Start, \DOp:=doIron, 0;
    
    ! Save restart data (e.g. the current state) 
    toolLast := tool; 
    wobjLast := currWobj;
    
    ! Start of clock for liftup and touchdown
    IF (liftup = TRUE OR touchdown = TRUE) THEN
    ClkReset printClock;
    ClkStart printClock;
    ENDIF
    
     IF (speed.v_tcp <> 0) THEN
        scale := nFlow / speed.v_tcp; ! scale = unitless, nFlow = mm/sec, v_tcp = mm/sec
    ENDIF
    TriggSpeed flowrate, 0\Start, 0, PLCOutputData2, 0;  
    
    IF Present(START) THEN
        
        ClkReset printClock; !Start of clock for backoff
        ClkStart printClock;
        IF compensate = TRUE THEN
            oriPos := pos;
            IF horizontal = TRUE THEN
                pos:= RelTool(prevPos,approach,0,0); !horizontal approach
            ELSE
				pos:= RelTool(prevPos,approach*Cos(30),0,-approach*Sin(30)); !30 degree approach - much more useful for true 3D
            ENDIF
                TriggIO triggStart, 0, \DOp:=doStart, 1;
        ELSE
            TriggIO triggStart, 0\Start, \DOp:=doStart, 1;
        ENDIF
        speed := backoffSpeed;
        
    ELSEIF Present(STOP) THEN
        !IF ZLC THEN extend := FALSE; ENDIF !if Zero Length Cutter is being used, no extension needed
        IF extend = TRUE THEN
            TriggIO triggStop, 0, \DOp:=doStop, 0;
        ELSE
            TriggIO triggStop, 0, \DOp:=doStop, 1;
        ENDIF
        
        
        
        
    ELSEIF Present(FIBERCUT) THEN
        TriggIO triggFiberCut, 0, \DOp:=doFiberCut , 1;
        
    ELSEIF Present(PREPRIME) THEN
        ClkReset printClock; !Start of clock for buildplate time
        ClkStart printClock;
        AM_Unwind pos.extax, speed, zone, tool,\wobj:=currWobj;
        ClkStop printClock; !End of clock for buildplate time
        buildplateTime := ClkRead(printClock); !buildplatetime
        ClkReset printClock; !Start of clock for travel time
        ClkStart printClock;
        MoveL pos, speed, fine, tool,\WObj:=currWobj;
        ClkStop printClock; !End of clock for travel time
        travelTime := ClkRead(printClock); !traveltime
        touchdown := TRUE;
        RETURN; 
   
    ENDIF
    
    IF anchorSlowDown = TRUE AND anchor = TRUE THEN
        waypoint1 := Lerp(prevPos, pos, anchorDistance, \first);
        waypoint2 := Lerp(waypoint1, pos, anchorDistance, \second);
        IF anchorSpeedFactor > 1 anchorSpeedFactor := 1;
        anchorSpeed := speed;
        anchorSpeed.v_tcp := speed.v_tcp * anchorSpeedFactor;
        MoveL waypoint1, anchorSpeed, zone, tool, \WObj:=currWobj;
        MoveL waypoint2, speed, zone, tool, \WObj:=currWobj;
        TriggL pos, anchorSpeed, flowrate, \T2:=triggFiberCut \T3:=triggStart \T4:=triggStop \T5:= triggIron, zone, tool, \WObj:=currWobj;
    ELSE
    TriggL pos, speed, flowrate, \T2:=triggFiberCut \T3:=triggStart \T4:=triggStop \T5:= triggIron, zone, tool, \WObj:=currWobj;
    ENDIF
    
    IF touchdown = TRUE THEN
        ClkStop printClock; !End of clock for touchdown
        touchdownTime := ClkRead(printClock); !touchdowntime
        touchdown := FALSE;
    ELSEIF liftup = TRUE THEN
        ClkStop printClock; !End of clock for liftup
        liftupTime := ClkRead(printClock); !liftuptime
        liftup := FALSE;
        strip:= strip+1;
        AM_CaptureTime;
    ENDIF
    
    prevPos := pos;
    
    IF Present(START) THEN
        anchor := TRUE;
        prevPos := oriPos;
        IF compensate = TRUE THEN
            speed.v_tcp := speed.v_tcp/backoffSpeedFactor;
            WaitRob \ZeroSpeed;
            ClkStop printClock; ! End of clock for Backoff
            backoffTime := ClkRead(printClock); !backofftime
            WaitDI diStartAck, 1, \MaxTime:=10 \TimeFlag:=startAcktimeout;
            CONNECT intClockStop WITH trapClockStop; !Start of clock for approach time
            TriggInt triggClockStop, 0, intClockStop;
            ClkReset printClock;
            ClkStart printClock;
            TriggL oriPos,speed,triggClockStop, zone,tool,\WObj:=currWobj; !End of clock for approach time
            
            approachtime := ClkRead(printClock); !approach time (clock stopped in the interrupt intClockStop)
            IDelete intClockStop;
        ELSE
            WaitDI diStartAck, 1, \MaxTime:=10 \TimeFlag:=startAcktimeout;
        ENDIF
        
        
    ELSEIF Present(STOP) THEN
        liftup := TRUE;
        TriggIO triggStop, 0, \DOp:=doStop, 1;
        CONNECT intClockStart WITH trapClockStart; !Start of clock for extension time
        TriggInt triggClockStart, 0\Start, intClockStart;
        TriggL RelTool(pos,-extension,0,0), speed, flowrate, \T2:=triggFiberCut \T3:=triggStart \T4:=triggStop \T5:= triggIron \T6:= triggClockStart, zone, tool, \WObj:=currWobj;
        WaitRob \ZeroSpeed;
        ClkStop printClock; !End of clock for extension time
        extensionTime := ClkRead(printClock); !extension time (clock started in the interrupt intClockStart)
        WaitDI diStopAck, 1, \MaxTime:=10 \TimeFlag:=stopAcktimeout;
        
        IDelete intClockStart;
        IDelete intClockStop;
        IF diPause = high THEN
            TPWrite "Pausing";
            ProcerrRecovery \SyncLastMoveInst; 
        ENDIF   
        
    ELSEIF Present(FIBERCUT) THEN
        anchor := FALSE;
        IF ZLC THEN !The sequence of Robot retract, wait for cut, go back to the part, Iron and move on..
			WaitTime ZLCRetractWaitTime;            
			MoveL RelTool(pos,-ZLCRetract*Cos(ZLCAngle),0,-ZLCRetract*Sin(ZLCAngle)), zlcRetractSpeed, zone, tool \WObj:=currWobj;
            WaitDI diFiberCutAck, 1, \MaxTime:=10 \TimeFlag:=cutAcktimeout;
            MoveL RelTool(pos,0,0,-5), zlcRetractSpeed, zone, tool \WObj:=currWobj;
            MoveL RelTool(pos,ironDist,0,-5), zlcRetractSpeed, zone, tool \WObj:=currWobj;
            TriggIO triggIron, 0, \DOp:= doIron, 1;
            TriggL RelTool(pos,ironDist,0,0), zlcRetractSpeed, flowrate, \T2:=triggFiberCut \T3:=triggStart \T4:=triggStop \T5:= triggIron, zone, tool, \WObj:=currWobj; !Backward
            MoveL pos, speed, zone, tool, \WObj:=currWobj; !Forward
        ELSE
            WaitDI diFiberCutAck, 1, \MaxTime:=10 \TimeFlag:=cutAcktimeout;
        ENDIF
    ENDIF
    
    IF startAcktimeout = TRUE THEN
    BookErrNo ERR_STARTACK;
    ErrRaise "ERR_STARTACK", startAckErrorid, startAckErrTitle, ERRSTR_TASK, startAckstr, ERRSTR_CONTEXT,ERRSTR_EMPTY;
    ENDIF
    
    IF cutAcktimeout = TRUE THEN
    BookErrNo ERR_CUTACK;
    ErrRaise "ERR_CUTACK", cutAckErrorid, cutAckErrTitle, ERRSTR_TASK, cutAckstr, ERRSTR_CONTEXT,ERRSTR_EMPTY;
    ENDIF
    
    IF stopAcktimeout = TRUE THEN
    BookErrNo ERR_STOPACK;
    ErrRaise "ERR_STOPACK", stopAckErrorid, stopAckErrTitle, ERRSTR_TASK, stopAckstr, ERRSTR_CONTEXT,ERRSTR_EMPTY;
   
    ENDIF
    
    
    
    ERROR
    ErrorNum := ERRNO;
   
    IF ERRNO = ERR_STARTACK THEN
        ErrorNum := ERRNO;
        ErrLog startAckErrorid, startAckstr,"","","","";
    ENDIF
    IF ERRNO = ERR_CUTACK THEN
        ErrorNum := ERRNO;
        ErrLog cutAckErrorid, cutAckstr, "","","","";
    ENDIF
    IF ERRNO = ERR_STOPACK THEN
        ErrorNum := ERRNO;
        ErrLog stopAckErrorid, stopAckstr,"","","","";
    ENDIF
    
    IF ERRNO = ERR_PATH_STOP AND diPause = high THEN
        strip:=strip+1;
        AM_CaptureTime;
        StorePath;
        pausePos := CRobT(\Tool:=tool,\WObj:=currWobj);
		stopPos := pausePos; 
        MoveL Offs(pausePos,0,0,100),v50,fine,tool\wobj:=currWobj;
        TPWrite "Paused";
        SetDO doPause, high;
        WaitDI diPause, low;
        SetDO doPause, low;
        MoveL pausePos,v50,fine,tool\wobj:=currWobj;
        strip:=strip-1;
        RestoPath;
        StartMoveRetry; 
    ELSE
        EXIT;
    ENDIF

ENDPROC

PROC AM_MoveC(\switch START | switch STOP | switch FIBERCUT | switch PREPRIME , num nFlow, robtarget waypos, robtarget pos, speeddata speed, zonedata zone, PERS tooldata tool, \PERS wobjdata wobj)
    
    VAR num epsilon := 0.01;
    VAR bool timeout;
    VAR bool compensate := TRUE;
    VAR bool extend := TRUE;
    VAR robtarget currPos;
    VAR robtarget postPos;
	VAR robtarget prePos;
    VAR bool margin;
    VAR speeddata backoffSpeed;
    VAR speedData zlcRetractSpeed;
    
    VAR triggdata triggFiberCut;
    VAR triggdata triggStart;
    VAR triggdata triggStop;
    VAR triggdata triggIron;
    VAR triggdata triggClockStop;
    VAR triggdata triggClockStart;
    
    VAR triggdata flow;
    VAR triggdata flowrate;
    VAR triggdata flowrateClone;
    VAR triggData TCPSpeed;

    VAR robtarget pausePos;
    VAR robtarget startPos;
    VAR robtarget toPos;
    VAR robtarget fromPos;
    VAR num scale := 1;
    
    
    IF Present(wobj) THEN
        currWobj := wobj;
    ELSE
        currWobj := wobj0;
    ENDIF
    
    backoffSpeed := speed;
    zlcRetractSpeed := speed;
    backoffSpeed.v_tcp := backoffSpeedFactor*speed.v_tcp;
    zlcRetractSpeed.v_tcp := zlcRetractSpeedFactor*speed.v_tcp;
    
    ! Offset the robot position by the user specified offset
    pos := Offs(pos, xOffset, yOffset, zOffset);
    pos.extax.eax_a := pos.extax.eax_a - extjOffset;
    pos.robconf := PrintConfData;
    
    waypos := Offs(waypos, xOffset, yOffset, zOffset);
    waypos.extax.eax_a := waypos.extax.eax_a - extjOffset;
    waypos.robconf := PrintConfData;
    
    TriggIO triggFiberCut, 0\Start, \DOp:=doFiberCut, 0;
    TriggIO triggStart, 0\Start, \DOp:=doStart, 0;
    TriggIO triggStop, 0\Start, \DOp:=doStop, 0;
    TriggIO triggIron, 0\Start, \DOp:=doIron, 0;
    
    ! Save restart data (e.g. the current state)
    toolLast := tool; 
    wobjLast := currWobj;
    
     IF (speed.v_tcp <> 0) THEN
        scale := nFlow / speed.v_tcp; ! scale = unitless, nFlow = mm/sec, v_tcp = mm/sec
    ENDIF
    TriggSpeed flowrate, 0\Start, 0, PLCOutputData2, 0;  
    
    IF Present(START) THEN
        ClkReset printClock;
        ClkStart printClock;
        IF compensate = TRUE THEN
            IF horizontal = TRUE THEN
                prepos:= RelTool(prevPos,approach,0,0); !horizontal approach
            ELSE
				prepos:= RelTool(prevPos,approach*Cos(30),0,-approach*Sin(30)); !30 degree approach - much more useful for true 3D
            ENDIF 
                
                TriggIO triggStart, 0, \DOp:=doStart, 1;
                TriggL prepos, backoffSpeed, flowrate, \T2:=triggFiberCut \T3:=triggStart \T4:=triggStop , zone, tool, \WObj:=currWobj;
        ELSE
            TriggIO triggStart, 0\Start, \DOp:=doStart, 1;
            TriggL prevPos, backoffSpeed, flowrate, \T2:=triggFiberCut \T3:=triggStart \T4:=triggStop , zone, tool, \WObj:=currWobj;
        ENDIF
        WaitRob \ZeroSpeed;
        ClkStop printClock;
        backoffTime := ClkRead(printClock); !backofftime
        WaitDI diStartAck, 1, \MaxTime:=10 \TimeFlag:=startAcktimeout;
        CONNECT intClockStop WITH trapClockStop;
        TriggInt triggClockStop, 0, intClockStop;
        ClkReset printClock;
        ClkStart printClock;
        TriggL prevPos,speed,triggClockStop, zone,tool,\WObj:=currWobj;
            
        approachtime := ClkRead(printClock); !approach time (clock stopped in the interrupt intClockStop)
        IDelete intClockStop;
        TriggIO triggStart, 0\Start, \DOp:=doStart, 0;
        
        
        
    ELSEIF Present(STOP) THEN
        !IF ZLC THEN extend := FALSE; ENDIF !if Zero Length Cutter is being used, no extension needed
        IF extend = TRUE THEN
            postpos:= RelTool(pos,-extension,0,0);
            TriggIO triggStop, 0, \DOp:=doStop, 0;
        ELSE
            TriggIO triggStop, 0, \DOp:=doStop, 1;
        ENDIF
        
        
    ELSEIF Present(FIBERCUT) THEN
        TriggIO triggFiberCut, 0, \DOp:=doFiberCut , 1;
        
    ELSEIF Present(PREPRIME) THEN
        ClkReset printClock;
        ClkStart printClock;
        AM_Unwind pos.extax, speed, zone, tool,\wobj:=currWobj;
        ClkStop printClock;
        buildplateTime := ClkRead(printClock); !buildplatetime
        ClkReset printClock;
        ClkStart printClock;
        MoveL pos, speed, fine, tool,\WObj:=currWobj;
        ClkStop printClock;
        travelTime := ClkRead(printClock); !traveltime
        touchdown := TRUE;
        RETURN; 
   
    ENDIF
    
    TriggC waypos, pos, speed, flowrate, \T2:=triggFiberCut \T3:=triggStart \T4:=triggStop , zone, tool, \WObj:=currWobj;  
    
    prevPos := pos;
    
    IF Present(START) THEN
        
    ELSEIF Present(STOP) THEN
        liftup := TRUE;
        IF extend = TRUE THEN
            TriggIO triggStop, 0, \DOp:=doStop, 1;
            CONNECT intClockStart WITH trapClockStart;
            TriggInt triggClockStart, 0\Start, intClockStart;
            TriggL RelTool(pos,-extension,0,0), speed, flowrate, \T2:=triggFiberCut \T3:=triggStart \T4:=triggStop \T5:= triggIron \T6:= triggClockStart, zone, tool, \WObj:=currWobj;
            WaitRob \ZeroSpeed;
            ClkStop printClock;
            extensionTime := ClkRead(printClock); !extension time (clock started in the interrupt intClockStart)        
            IDelete intClockStart;
            IDelete intClockStop;
            WaitDI diStopAck, 1, \MaxTime:=10 \TimeFlag:=stopAcktimeout;
        ELSE
            WaitDI diStopAck, 1, \MaxTime:=10 \TimeFlag:=stopAcktimeout;
            extensionTime := 0;
        ENDIF
        
        IF diPause = high THEN
            TPWrite "Pausing";
            ProcerrRecovery \SyncLastMoveInst; 
        ENDIF   
        
    ELSEIF Present(FIBERCUT) THEN
        IF ZLC THEN !The sequence of Robot retract, wait for cut, go back to the part, Iron and move on..
			WaitTime ZLCRetractWaitTime;
            MoveL RelTool(pos,-ZLCRetract*Cos(ZLCAngle),0,-ZLCRetract*Sin(ZLCAngle)), zlcRetractSpeed, zone, tool \WObj:=currWobj;
            WaitDI diFiberCutAck, 1, \MaxTime:=10 \TimeFlag:=cutAcktimeout;
            MoveL RelTool(pos,0,0,-5), zlcRetractSpeed, zone, tool \WObj:=currWobj;
            MoveL RelTool(pos,ironDist,0,-5), speed, zone, tool \WObj:=currWobj;
            TriggIO triggIron, 0, \DOp:= doIron, 1;
            TriggL RelTool(pos,ironDist,0,0), speed, flowrate, \T2:=triggFiberCut \T3:=triggStart \T4:=triggStop \T5:= triggIron, zone, tool, \WObj:=currWobj; !Backward
            MoveL pos, speed, zone, tool, \WObj:=currWobj; !Forward
        ELSE
            WaitDI diFiberCutAck, 1, \MaxTime:=10 \TimeFlag:=cutAcktimeout;
        ENDIF
    ENDIF
    
    IF startAcktimeout = TRUE THEN
    BookErrNo ERR_STARTACK;
    ErrRaise "ERR_STARTACK", startAckErrorid, startAckErrTitle, ERRSTR_TASK, startAckstr, ERRSTR_CONTEXT,ERRSTR_EMPTY;
    ENDIF
    
    IF cutAcktimeout = TRUE THEN
    BookErrNo ERR_CUTACK;
    ErrRaise "ERR_CUTACK", cutAckErrorid, cutAckErrTitle, ERRSTR_TASK, cutAckstr, ERRSTR_CONTEXT,ERRSTR_EMPTY;
    ENDIF
    
    IF stopAcktimeout = TRUE THEN
    BookErrNo ERR_STOPACK;
    ErrRaise "ERR_STOPACK", stopAckErrorid, stopAckErrTitle, ERRSTR_TASK, stopAckstr, ERRSTR_CONTEXT,ERRSTR_EMPTY;
    ENDIF
    
    
    
    ERROR
    ErrorNum := ERRNO;
   
    IF ERRNO = ERR_STARTACK THEN
        ErrorNum := ERRNO;
        ErrLog startAckErrorid, startAckstr,"","","","";
    ENDIF
    IF ERRNO = ERR_CUTACK THEN
        ErrorNum := ERRNO;
        ErrLog cutAckErrorid, cutAckstr, "","","","";
    ENDIF
    IF ERRNO = ERR_STOPACK THEN
        ErrorNum := ERRNO;
        ErrLog stopAckErrorid, stopAckstr,"","","","";
    ENDIF
    
    IF ERRNO = ERR_PATH_STOP AND diPause = high THEN
        strip:=strip+1;
        AM_CaptureTime;
        StorePath;
        pausePos := CRobT(\Tool:=tool,\WObj:=currWobj);
		stopPos := pausePos; 
        MoveL Offs(pausePos,0,0,100),v50,fine,tool\wobj:=currWobj;
        TPWrite "Paused";
        SetDO doPause, high;
        WaitDI diPause, low;
        SetDO doPause, low;
        MoveL pausePos,v50,fine,tool\wobj:=currWobj;
        strip:=strip-1;
        RestoPath;
        StartMoveRetry; 
    ELSE
        EXIT;
    ENDIF

ENDPROC

PROC AM_ScanL(\switch START | switch STOP | switch PREPRIME , robtarget pos, speeddata speed, zonedata zone, PERS tooldata tool, \PERS wobjdata wobj)
    
    VAR num epsilon := 0.01;
    VAR bool timeout;
    VAR robtarget currPos;
    VAR robtarget oriPos;
    VAR bool margin;
    
    
    VAR triggdata triggFiberCut;
    VAR triggdata triggStart;
    VAR triggdata triggStop;
    
    VAR triggdata flow; 
    VAR triggdata flowrate;
    VAR triggdata flowrateClone;
    VAR triggData TCPSpeed;

    VAR robtarget pausePos;
    VAR robtarget startPos;
    VAR robtarget toPos;
    VAR robtarget fromPos;
    VAR num scale := 1;
    VAR num Distance := 0;
    !Accset 50,50;
    IF Present(wobj) THEN
        currWobj := wobj;
    ELSE
        currWobj := wobj0;
    ENDIF
    
    ! Offset the robot position by the user specified offset
    pos := Offs(pos, xOffset, yOffset, zOffset);
   	pos.extax.eax_a := pos.extax.eax_a - extjOffset;
    pos.robconf := scanConfData;
    
    TriggIO triggStart, 0\Start, \DOp:=doStart, 0;
    TriggIO triggStop, 0\Start, \DOp:=doStop, 0;
    
    ! Save restart data (e.g. the current state)
    toolLast := tool; 
    wobjLast := currWobj;
    
    IF Present(START) THEN
        TriggIO triggStart, 0\Start, \DOp:=doStart, 1;
        
        
    ELSEIF Present(STOP) THEN
        TriggIO triggStop, 0, \DOp:=doStop, 1;
        
    ELSEIF Present(PREPRIME) THEN
        AM_Unwind pos.extax, speed, zone, tool,\wobj:=wobj;
        MoveL pos, speed, fine, tool,\WObj:=wobj;
        RETURN; 
   
    ENDIF
    
    TriggL pos, speed, triggStart \T2:=triggStop , zone, tool, \WObj:=wobj;  
    
    prevPos := pos;
    
    IF Present(START) THEN
        WaitDI diStartAck, high, \MaxTime:=10 \TimeFlag:=startAcktimeout;
        SetDO doStart, low;
        
    ELSEIF Present(STOP) THEN
        WaitDI diStopAck, high, \MaxTime:=10 \TimeFlag:=stopAcktimeout;
        SetDO doStop, low;
        IF diPause = high THEN
            TPWrite "Pausing";
            ProcerrRecovery \SyncLastMoveInst; 
        ENDIF   
        
    ENDIF
    
    IF startAcktimeout = TRUE THEN
        BookErrNo ERR_STARTACK;
        ErrRaise "ERR_STARTACK", startAckErrorid, startAckErrTitle, ERRSTR_TASK, startAckstr, ERRSTR_CONTEXT,ERRSTR_EMPTY;
    ENDIF
    
    IF stopAcktimeout = TRUE THEN
        BookErrNo ERR_STOPACK;
        ErrRaise "ERR_STOPACK", stopAckErrorid, stopAckErrTitle, ERRSTR_TASK, stopAckstr, ERRSTR_CONTEXT,ERRSTR_EMPTY;
    ENDIF
    
    ERROR
    ErrorNum := ERRNO;
   
    IF ERRNO = ERR_STARTACK THEN
        ErrorNum := ERRNO;
        ErrLog startAckErrorid, startAckstr,"","","","";
    ENDIF
    IF ERRNO = ERR_STOPACK THEN
        ErrorNum := ERRNO;
        ErrLog stopAckErrorid, stopAckstr,"","","","";
    ENDIF
    
    IF ERRNO = ERR_PATH_STOP AND diPause = high THEN
        StorePath;
        pausePos := CRobT(\Tool:=tool,\WObj:=wobj);
        MoveL Offs(pausePos,0,0,100),v50,fine,tool\wobj:=wobj;
        TPWrite "Paused";
        SetDO doPause, high;
        WaitDI diPause, low;
        SetDO doPause, low;
        MoveL pausePos,v50,fine,tool\wobj:=wobj;
        RestoPath;
        StartMoveRetry; 
    ELSE
        EXIT;
    ENDIF

ENDPROC


PROC AM_ScanC(\switch START | switch STOP | switch PREPRIME , robtarget waypos, robtarget pos, speeddata speed, zonedata zone, PERS tooldata tool, \PERS wobjdata wobj)
    
    VAR num epsilon := 0.01;
    VAR bool timeout;
    VAR robtarget currPos;
    VAR robtarget oriPos;
    VAR bool margin;
    
    
    VAR triggdata triggFiberCut;
    VAR triggdata triggStart;
    VAR triggdata triggStop;
    
    VAR triggdata flow; 
    VAR triggdata flowrate;
    VAR triggdata flowrateClone;
    VAR triggData TCPSpeed;

    VAR robtarget pausePos;
    VAR robtarget startPos;
    VAR robtarget toPos;
    VAR robtarget fromPos;
    VAR num scale := 1;
    VAR num Distance := 0;
    !Accset 50,50;
    IF Present(wobj) THEN
        currWobj := wobj;
    ELSE
        currWobj := wobj0;
    ENDIF
    
    ! Offset the robot position by the user specified offset
    pos := Offs(pos, xOffset, yOffset, zOffset);
   	pos.extax.eax_a := pos.extax.eax_a - extjOffset;
    pos.robconf := scanConfData;
    
    waypos := Offs(waypos, xOffset, yOffset, zOffset);
   	waypos.extax.eax_c := waypos.extax.eax_c - extjOffset;
    waypos.robconf := scanConfData;
    
    TriggIO triggStart, 0\Start, \DOp:=doStart, 0;
    TriggIO triggStop, 0\Start, \DOp:=doStop, 0;
    
    ! Save restart data (e.g. the current state)
    toolLast := tool; 
    wobjLast := currWobj;
    
    IF Present(START) THEN
        TriggIO triggStart, 0\Start, \DOp:=doStart, 1;
        
        
    ELSEIF Present(STOP) THEN
        TriggIO triggStop, 0, \DOp:=doStop, 1;
        
    ELSEIF Present(PREPRIME) THEN
        AM_Unwind pos.extax, speed, zone, tool,\wobj:=wobj;
        MoveL pos, speed, fine, tool,\WObj:=wobj;
        RETURN; 
   
    ENDIF
    
    TriggC waypos, pos, speed, triggStart \T2:=triggStop , zone, tool, \WObj:=wobj;  
    
    prevPos := pos;
    
    IF Present(START) THEN
        WaitDI diStartAck, high, \MaxTime:=10 \TimeFlag:=startAcktimeout;
        SetDO doStart, low;
        
    ELSEIF Present(STOP) THEN
        WaitDI diStopAck, high, \MaxTime:=10 \TimeFlag:=stopAcktimeout;
        SetDO doStop, low;
        IF diPause = high THEN
            TPWrite "Pausing";
            ProcerrRecovery \SyncLastMoveInst; 
        ENDIF   
        
    ENDIF
    
    IF startAcktimeout = TRUE THEN
        BookErrNo ERR_STARTACK;
        ErrRaise "ERR_STARTACK", startAckErrorid, startAckErrTitle, ERRSTR_TASK, startAckstr, ERRSTR_CONTEXT,ERRSTR_EMPTY;
    ENDIF
    
    IF stopAcktimeout = TRUE THEN
        BookErrNo ERR_STOPACK;
        ErrRaise "ERR_STOPACK", stopAckErrorid, stopAckErrTitle, ERRSTR_TASK, stopAckstr, ERRSTR_CONTEXT,ERRSTR_EMPTY;
    ENDIF
    
    ERROR
    ErrorNum := ERRNO;
   
    IF ERRNO = ERR_STARTACK THEN
        ErrorNum := ERRNO;
        ErrLog startAckErrorid, startAckstr,"","","","";
    ENDIF
    IF ERRNO = ERR_STOPACK THEN
        ErrorNum := ERRNO;
        ErrLog stopAckErrorid, stopAckstr,"","","","";
    ENDIF
    
    IF ERRNO = ERR_PATH_STOP AND diPause = high THEN
        StorePath;
        pausePos := CRobT(\Tool:=tool,\WObj:=wobj);
        MoveL Offs(pausePos,0,0,100),v50,fine,tool\wobj:=wobj;
        TPWrite "Paused";
        SetDO doPause, high;
        WaitDI diPause, low;
        SetDO doPause, low;
        MoveL pausePos,v50,fine,tool\wobj:=wobj;
        RestoPath;
        StartMoveRetry; 
    ELSE
        EXIT;
    ENDIF

ENDPROC

PROC TeachPend()
    TPWrite "TCP-speed: "+NumToStr(AOutput(PLCOutputData2), 4)+" mm/s";
ENDPROC

PROC AM_Unwind(extjoint extj, speeddata speed, zonedata zone, PERS tooldata tool, \PERS wobjdata wobj)
    VAR jointtarget current;
    VAR jointtarget rotate;
    current := CJointT();
    rotate.robax := current.robax;
    rotate.extax := extj;
    MoveAbsJ rotate,speed, fine, tool;
ENDPROC

PROC AM_CaptureTime()
   
    stripTimes.stripnum := strip;
    stripTimes.touchdownTime := touchdownTime;
    stripTimes.backoffTime := backoffTime;
    stripTimes.approachTime := approachTime;
    stripTimes.extensionTime := extensionTime;
    stripTimes.liftupTime := liftupTime;
    stripTimes.buildplateTime := buildplateTime;
    stripTimes.travelTime := travelTime;
ENDPROC



PROC AM_PartRegistration()
    VAR pos marks{8} := [[446,0,74.39],[398.5,0,73.53],[300,65,73.08],[300,-70,73.34],[46.34,-57.08,24.50],[-67.85,-51.5,25.55],[-169.08,-46.55,25.55],[46.34,29.19,25.55]];
    VAR pos currentMark := [0,0,0];
    VAR extjoint ext := [9E9,0,15,9E9,9E9,9E9];
    SingArea \Wrist;
    ConfL \On;
    ext.eax_c := ext.eax_c - Euz;
    activeTool := tool_Milling;
    activeWobj := wobj_rotBP;
    FOR i FROM 1 TO 7 DO
        currentMark := marks{i};
        
        ! Move to new point at safe height
        currentMark.z := 300;
        MoveL [currentMark, [0,0,-1,0],[-1,-1,-1,1],ext], v100, fine, activeTool\WObj:=activeWobj;  
        
        ! Move to point at low height
        currentMark.z := marks{i}.z + 10;
        MoveL [currentMark, [0,0,-1,0],[-1,-1,-1,1],ext], v100, fine, activeTool\WObj:=activeWobj;
        
        ! Move to point at drill depth
        currentMark.z := marks{i}.z - 1.5;
        MoveL [currentMark, [0,0,-1,0],[-1,-1,-1,1],ext], v5, fine, activeTool\WObj:=activeWobj;
        
        ! Return to point at safe height
        currentMark.z := 300;
        MoveL [currentMark, [0,0,-1,0],[-1,-1,-1,1],ext], v100, fine, activeTool\WObj:=activeWobj;
    ENDFOR
    ConfL \Off;
ENDPROC

ENDMODULE
