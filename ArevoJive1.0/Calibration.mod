MODULE Calibration
!!!----------------------------------********---------------------------------------------!!!
! Company Confidential
! ArevoJive Calibration Version 1.0
! Generated at Arevo | 8/3/2018 | Author - Archana Kashikar, Armando Armijo
! Purpose - This module contains calibration procedures and functions used for calibrating the 
!           tools and woek objects used by Arevo Printers
!           Calibration.MOD performs calibration for rotating buildplatform, static build platform,
!           Direct Energy Deposition tool, Renishaw tools
!
! Changes-
!!!----------------------------------********---------------------------------------------!!!
    
    !=================VARIABLE OR CONSTANT DECLARATION==========================!
    
    RECORD posDnum
        dnum x;
        dnum y;
        dnum z;
    ENDRECORD
    
    RECORD posAndNum
        pos position;
        num number;
        
    ENDRECORD
    
    PERS posdnum centerpointDnum{8};
    VAR posdnum sp1dnum;
    VAR posdnum sp2dnum;
    VAR posdnum sp3dnum;
    VAR posdnum sp4dnum;
    
    CONST robtarget robTargetEmptyDown := [[0,0,0],[0,0,-1,0],[0,0,0,0],[0,9E9,9E9,9E9,9E9,9E9]];
    CONST robtarget robTargetEmptyUp := [[0,0,0],[1,0,0,0],[0,0,0,0],[0,9E9,9E9,9E9,9E9,9E9]];
    
    !------------Touch Probe Calibration-------------!
    VAR robtarget touch_point := [[214,-630,250],[0,-1,0,0],[0,0,0,0],[9E9,9E9,9E9,9E9,9E9,9E9]];
    VAR robtarget tp;
    
    !--------Touch Probe Repeatability Test in z-------!
    VAR robtarget touchpointz := [[16,10,100],[0,-1,0,0],[0,0,0,0],[9E9,9E9,9E9,9E9,9E9,9E9]];
    PERS pos touchpoints{10} :=[[0.0369582,501.59,-1.00038],[0.0359881,501.593,-0.99452],[0.0351203,501.593,-0.996594],[0.036273,501.598,-0.99452],[0.0357705,501.589,-0.993463],[0.0374071,501.59,-0.995563],[0.0352819,501.592,-0.994519],[0.0357188,501.587,-0.99765],[0.0351203,501.593,-0.99452],[0.0358422,501.59,-0.995575]];
    
    !--------Touch Probe Repeatability Test in x&y-----!
    VAR robtarget touchpointxy := [[0,560,-8.5],[0,-1,0,0],[0,0,0,0],[9E9,9E9,9E9,9E9,9E9,9E9]];
    
    !--------Build Plate Planarity sampling using touch probe-----!
    CONST num z :=100;
    CONST pos p{59} := [[-444.33,-119.06,z], [-444.33,119.06,z], [-347.73,-93.17,z], [-347.73,93.17,z], [-325.27,-325.27,z], [-325.27,325.27,z], [-270.46,-72.47,z], [-270.46,72.47,z], [-254.56,-254.56,z], [-254.56,254.56,z], [-197.99,-197.99,z], [-197.99,197.99,z], [-173.87,-46.59,z], [-173.87,46.59,z], [-127.28,-127.28,z], [-127.28,127.28,z], [-119.06,-444.33,z], [-119.06,444.33,z], [-115.91,-31.06,z], [-115.91,31.06,z], [-93.17,-347.73,z], [-93.17,347.73,z], [-84.85,-84.85,z], [-84.85,84.85,z], [-72.47,-270.46,z], [-72.47,270.46,z], [-46.59,173.87,z], [-31.06,-115.91,z], [-31.06,115.91,z], [31.06,-115.91,z], [31.06,115.91,z], [46.59,-173.87,z], [46.59,173.87,z], [72.47,-270.46,z], [72.47,270.46,z], [84.85,-84.85,z], [84.85,84.85,z], [93.17,-347.73,z], [93.17,347.73,z], [115.91,-31.06,z], [115.91,31.06,z], [119.06,-444.33,z], [119.06,444.33,z], [127.28,-127.28,z], [127.28,127.28,z], [173.87,-46.59,z], [173.87,46.59,z], [197.99,-197.99,z], [197.99,197.99,z], [254.56,-254.56,z], [254.56,254.56,z], [270.46,-72.47,z], [270.46,72.47,z], [325.27,-325.27,z], [325.27,325.27,z], [347.73,-93.17,z], [347.73,93.17,z], [444.33,-119.06,z], [444.33,119.06,z]];
    PERS pos buildplatepoints{59}:= [[-444.281,-119.069,-0.406878],[-444.36,119.056,-0.393573],[-347.751,-93.1659,-0.602184], [-347.805,93.1679,-0.606993], [-325.227,-325.28,-0.396333], [-325.238,325.282,-0.465982], [-270.409,-72.4718,-0.651711], [-270.483,72.4725,-0.648462], [-254.515,-254.57,-0.605969], [-254.604,254.552,-0.69714], [-198.04,-197.979,-0.67563], [-197.97,197.994,-0.729343], [-173.821,-46.5934,-0.772224], [-173.845,46.5964,-0.780742], [-127.282,-127.28,-0.728423], [-127.33,127.277,-0.7923], [-119.066,-444.322,-0.504121], [-119.025,444.343,-0.670242], [-115.898,-31.0589,-0.800952], [-115.867,31.0611,-0.782819], [-93.1593,-347.731,-0.623568], [-93.1779,347.727,-0.770864], [-84.9055,-84.8444,-0.822974], [-84.7944,84.8533,-0.818383], [-72.4392,-270.463,-0.723144], [-72.4274,270.47,-0.807418], [-46.5313,173.88,-0.851003], [-31.123,-115.906,-0.820188], [-31.1185,115.906,-0.838458], [31.0179,-115.905,-0.821027], [30.9981,115.907,-0.847607], [46.6439,-173.875,-0.722101], [46.6316,173.878,-0.810677], [72.4178,-270.45,-0.77731], [72.471,270.46,-0.908185], [84.792,-84.8398,-0.872336], [84.8791,84.854,-0.894677], [93.1142,-347.714,-0.655781], [93.1213,347.72,-0.875986], [115.943,-31.0556,-0.999808], [115.862,31.0639,-0.974479], [119.05,-444.328,-0.487728], [119.091,444.339,-0.777156], [127.275,-127.278,-0.901127], [127.231,127.278,-0.958569], [173.876,-46.5876,-0.871905], [173.847,46.5938,-0.897445], [197.95,-197.987,-0.753096], [198.021,197.993,-0.903653], [254.531,-254.552,-0.738816], [254.534,254.556,-0.938167], [270.42,-72.4651,-0.888989], [270.466,72.479,-0.955288], [325.242,-325.263,-0.533989], [325.305,325.277,-0.793655], [347.744,-93.1667,-0.755732], [347.761,93.1744,-0.85569], [444.285,-119.048,-0.8065], [444.362,119.065,-0.934767]];
    PERS pos buildplatepoints2{59}:= [[-444.279,-119.069,-0.396102],[-444.364,119.055,-0.389461],[-347.752,-93.1661,-0.615764], [-347.805,93.1688,-0.619088], [-325.226,-325.28,-0.389083], [-325.244,325.281,-0.480599], [-270.411,-72.4699,-0.647496], [-270.482,72.4694,-0.637007], [-254.518,-254.571,-0.598144], [-254.601,254.553,-0.68841], [-198.039,-197.979,-0.666307], [-197.972,197.991,-0.724627], [-173.82,-46.5935,-0.768776], [-173.844,46.5972,-0.796879], [-127.286,-127.279,-0.731808], [-127.335,127.277,-0.774544], [-119.07,-444.326,-0.491706], [-119.028,444.342,-0.676303], [-115.899,-31.0605,-0.784549], [-115.864,31.0611,-0.787072], [-93.1551,-347.732,-0.640887], [-93.1763,347.727,-0.753252], [-84.9009,-84.8444,-0.825413], [-84.7979,84.8512,-0.825879], [-72.4379,-270.464,-0.721564], [-72.4245,270.468,-0.803114], [-46.5342,173.88,-0.85308], [-31.1253,-115.908,-0.819303], [-31.1185,115.908,-0.858487], [31.0213,-115.907,-0.834212], [31.0067,115.907,-0.857263], [46.6403,-173.875,-0.724293], [46.6301,173.882,-0.78959], [72.4243,-270.451,-0.789838], [72.4748,270.462,-0.898474], [84.7994,-84.8425,-0.874328], [84.8816,84.8541,-0.900459], [93.1148,-347.714,-0.648599], [93.119,347.721,-0.870729], [115.945,-31.0557,-0.99005], [115.861,31.062,-0.985191], [119.051,-444.33,-0.470804], [119.085,444.342,-0.76942], [127.275,-127.277,-0.889761], [127.232,127.279,-0.966985], [173.878,-46.5876,-0.881242], [173.849,46.5938,-0.886918], [197.948,-197.984,-0.765565], [198.015,197.993,-0.902229], [254.532,-254.55,-0.752423], [254.534,254.557,-0.946506], [270.418,-72.4642,-0.880982], [270.467,72.4744,-0.948345], [325.243,-325.266,-0.529523], [325.304,325.28,-0.791786], [347.745,-93.1678,-0.753355], [347.759,93.1727,-0.859758], [444.29,-119.051,-0.792845], [444.365,119.065,-0.929215]];
    !------------Calibration of Thru Beam------------------!
    CONST robtarget thruBeamCalibHome := [[576.60,230.22,176],[0,0.70711,-0.70711,0],[0,0,-2,1],[0,9E+09,9E+09,9E+09,9E+09,9E+09]];
    TASK PERS pos thrubeamA:=[576.597,297.084,183.542];
    !----------------------------Calibration of TCP using Thru beam---------------!
    PERS robtarget tcpcalibhome1:=[[576.597,227.084,178.542],[0,0.70711,-0.70711,0],[0,-1,-2,1],[0,9E+09,9E+09,9E+09,9E+09,9E+09]];
    PERS robtarget tcpcalibhome2:=[[576.597,227.084,178.542],[0,0,1,0],[0,0,-1,1],[0,9E+09,9E+09,9E+09,9E+09,9E+09]];
    TASK PERS pos offseta := [-0.0150452,-0.00112915,0];
    TASK PERS pos offsetb := [-0.239899,0.166565,472.462];
    
    
    PERS bool calibOrient := FALSE;
    !--------------Calibration of worktable rotation center using touch probe---------------!
    PERS robtarget point2:=[[1281.7,-173.205,370],[0,-0.5,0.866025,0],[0,0,0,0],[9E+09,0,120,9E+09,9E+09,9E+09]];
    PERS robtarget point3:=[[1281.7,173.205,370],[0,0.5,0.866025,0],[0,0,0,0],[9E+09,0,240,9E+09,9E+09,9E+09]];
    PERS robtarget point1:=[[1581.7,0,370],[0,-1,0,0],[0,0,0,0],[9E+09,0,0,9E+09,9E+09,9E+09]];
    
    !----------------Tool z Calibration using touch probe-----------------------------------!
    CONST robtarget p10:=[[-596.34,-1367.03,584.22],[0.000208689,0.706985,2.06483E-05,-0.707229],[-2,-4,-1,1],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget p20:=[[-377.07,-1371.69,585.58],[0.707106781,0,-0.707106781,0],[-2,-2,-3,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    
    !---------------Calibration of buildplate using Renishaw and spherical ball-----------------------------!
    !----User input----!
    PERS robtarget center:=[[1396.55, 13.1912, 384.255],[0,0,1,0],[0,0,0,0],[0,9E+09,9E+09,9E+09,9E+09,9E+09]];
    PERS robtarget rotPoint1:=[[1697.94,21.33,493.78],[7.74877E-06,-0.22674,-0.973955,6.96103E-06],[0,-1,0,0],[0.000271339,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget verifyRotCalibPos := [[-13.25, -11.80, 442.74],[0,0,-1,0],[0,0,0,0],[0,9E+09,9E+09,9E+09,9E+09,9E+09]];
    PERS num minimumBPZ := 380;
    
    !----End of User Input----!
    PERS dnum PP{4,3} := [[1623.68627929688,-195.592727661133,480.544769287109],[1599.23291015625,-195.594879150391,480.528411865234],[1611.39038085938,-183.408813476563,480.464385986328],[1611.43383789063,-195.628692626953,493.012634277344]];
    PERS robtarget rotPoints{8};
    PERS robtarget centerPoints{8};
    LOCAL VAR robtarget sp1;
    LOCAL VAR robtarget sp2;
    LOCAL VAR robtarget sp3;
    LOCAL VAR robtarget sp4;
    LOCAL VAR robtarget sp5;
    LOCAL VAR robtarget sp6;
    LOCAL VAR robtarget sp7;
    PERS robtarget rotPoint2:=[[1606.71,232.19,381.42],[8.86461E-05,-0.226757,-0.973951,3.47202E-05],[0,-1,0,0],[45,9E+09,9E+09,9E+09,9E+09,9E+09]];
    PERS robtarget rotPoint3:=[[1394.33,317.78,380.07],[7.1917E-05,-0.226822,-0.973936,2.83378E-05],[0,0,0,0],[89.9997,9E+09,9E+09,9E+09,9E+09,9E+09]];
    PERS robtarget rotPoint4:=[[1183.41,225.61,378.22],[8.86127E-05,-0.226848,-0.97393,2.27796E-05],[0,0,0,0],[135,9E+09,9E+09,9E+09,9E+09,9E+09]];
    PERS robtarget rotPoint5:=[[1097.68,13.00,375.82],[7.40275E-05,-0.226836,-0.973933,2.61013E-05],[0,-1,0,0],[180,9E+09,9E+09,9E+09,9E+09,9E+09]];
    PERS robtarget rotPoint6:=[[1189.09,-196.43,374.74],[8.20382E-05,-0.226817,-0.973937,4.07241E-05],[-1,-1,0,0],[225,9E+09,9E+09,9E+09,9E+09,9E+09]];
    PERS robtarget rotPoint7:=[[1402.66,-282.21,374.65],[9.93769E-05,-0.226833,-0.973934,5.27863E-05],[-1,-1,0,0],[270,9E+09,9E+09,9E+09,9E+09,9E+09]];
    PERS robtarget rotPoint8:=[[1612.37,-191.28,377.62],[0.000150066,-0.226881,-0.973922,5.65814E-05],[-1,-1,0,0],[315.001,9E+09,9E+09,9E+09,9E+09,9E+09]];
    PERS posAndNum circleData;
    !---------------Calibration of using Renishaw and spherical ball-----------------------------!
    !----User input----!
    PERS robtarget renishawToolSetter170Ruby:=[[1717.97,-19.2141,652.477],[0,0,-1,0],[0,-1,0,0],[0,9E+09,9E+09,9E+09,9E+09,9E+09]];
    
    !----End of User Input----!
    
    !-----------------Placeholder tools and wobjs------------------!
!    PERS tooldata activeTool := [TRUE,[[19.5867,53.0547,233.533],[0.774832,-0.256387,0.33357,0.471838]],[2.2,[0,0,10],[1,0,0,0],0,0,0]];
!    PERS wobjdata activeWobj := [FALSE,TRUE,"",[[810,312,234.17],[1,0,0,0]],[[-2.02905,-0.0482019,0.131684],[1,0,0,0]]];
!    PERS wobjdata staticWobj_calib:=[FALSE, TRUE,"",[[1380.48, -15.8397, 393.314],[0.999998,-0.00202798,-0.000732422,0]],[[0,0,0],[1,0,0,0]]];
!    PERS tooldata tool_calib := [TRUE,[[21.0107,51.1484,233.631],[0.777704,-0.2438,0.33952,0.469537]],[2.2,[0,0,10],[1,0,0,0],0,0,0]];
!    PERS wobjdata rotWobj_calib:=[FALSE,FALSE,"M7DM1",[[0,0,0],[1,0,0,0]],[[0,0,0],[1,0,0,0]]];
    
        !-----------------Placeholder tools and wobjs------------------!
    !LOCAL PERS wobjdata activeWobj:=[FALSE, TRUE,"",[[0, 0, 0],[1,0,0,0]],[[0,0,0],[1,0,0,0]]];
    !LOCAL PERS tooldata activeTool := [TRUE,[[-0.280208,-2.6716,324.448],[1,0,0,0]],[0.001,[0,0,0.001],[1,0,0,0],0,0,0]];
    LOCAL PERS wobjdata calibWobj:=[FALSE, FALSE,"M7DM1",[[0, 0, 0],[1,0,0,0]],[[0,0,0],[1,0,0,0]]];
    LOCAL PERS tooldata calibTool := [TRUE,[[21.0107,51.1484,233.631],[0.777704,-0.2438,0.33952,0.469537]],[2.2,[0,0,10],[1,0,0,0],0,0,0]];
    
    !-------data for debug-----------------!
    PERS pose frameDebug := [[946.556, -436.813,379.602],[0.999986, 0.00427985, -0.00303505, 1.34368E-05]];
    PERS pose rotFrame := [[1396.55,13.1912,382.904],[0.999963,0.00437194,-0.00305595,0.00673053]];
    PERS pos centerdebug;
    PERS num d;
    PERS pos centerPos;
    
    !--------------Signal Aliases---------------!
    VAR signaldo doRenishawPower;
    VAR signaldo doRenishawProbe;
    VAR signaldo doRenishawToolSetter;
    VAR signaldi diRenishawProbeStatus;
    VAR signaldi diRenishawToolSetterStatus;
    VAR signaldi diRenishawBatteryStatus;
    VAR signaldi diRenishawError;
    
    !-----------------Error Handling------------------!
    VAR errnum ERR_RENISHAW:=-1;
    VAR num rotWobjCalibErrId := 4804;
    VAR num renishawErrId := 4805;
    VAR errstr renishawErrTitle := "Renishaw Error";
    VAR errstr renishawErrStr := "Renishaw Error";
    
    PROC RenishawSignalAlias()
        ALIASIO doEthCat4, doRenishawPower;
        ALIASIO doEthCat1, doRenishawProbe;
        ALIASIO doEthCat2, doRenishawToolSetter;
        ALIASIO diEthCat1, diRenishawProbeStatus;
        ALIASIO diEthCat2, diRenishawToolSetterStatus;
        ALIASIO diEthCat3, diRenishawBatteryStatus;
        ALIASIO diEthCat4, diRenishawError;
    ENDPROC
    
    
    
    !================CALIBRATION PROCEDURE CALLS===============!
    
    !----------- BUILDPLATE CALIBRATION ----------!
    
    !Static Build Plate Calibration using Touch Probe
    PROC pStaticWorkObjectCalibration()
        VAR bool boolstaticCalib;
        
        activeWobj := wobj0;
        
        activeTool := tool_RenishawProbe;
        
        calibWobj := wobj_staticBP;
        
        boolstaticCalib := fStaticWorkObjCalib(calibWobj, center, 900);
        
        IF boolstaticCalib = FALSE THEN
            TPWrite "static Calibration could not be performed";
        ELSE
            TPWrite "static Calibration Successful";
            wobj_staticBP := calibWobj; 
        ENDIF
    ENDPROC
    
    !Rotating Build Plate Calibration using Touch Probe
    PROC pRotWorkObjectCalibration()
        VAR bool boolRotCalib;
        
        activeWobj := wobj0;
        
        activeTool := tool_RenishawProbe;
        
        calibWobj := wobj_rotBP;
        
        boolRotCalib := fRotWorkObjectCalibSphere(activeWobj, 600);
        
        IF boolRotCalib = FALSE THEN
            TPWrite "Rotation Calibration could not be performed";
        ELSE
            TPWrite "Rotation Calibration Successful";
            wobj_rotBP := calibWobj; 
            writeCfgToController(rotFrame);
        ENDIF
    ENDPROC
    
    !Rotating Build Plate Verification using Touch Probe
    PROC pRotWorkObjectVerification()
        VAR bool boolRotVerify;
        
        boolRotVerify := fRotWorkObjectVerification();
        
        
        IF boolRotVerify = FALSE THEN
            TPWrite "Rotation Verification could not be performed";
        ELSE
            TPWrite "Rotation Verification Successful";
        ENDIF
    ENDPROC
    
    
    !---------- TOOL CALIBRATION ------------!
    
     !Tool Calibration using tool setter
    PROC pRenishawRubyCalibration()
        VAR bool boolToolCalib;
        
        activeWobj := wobj_MasterRing;
        
        activeTool := tool_RenishawProbe;
        
        boolToolCalib := fRenishawRubyCalibration();
        
        IF boolToolCalib = FALSE THEN
            TPWrite "Renishaw Ruby Touch Probe Tool Calibration could not be performed";
        ELSE
            TPWrite "Renishaw Ruby Touch Probe Tool Calibration Successful";
            tool_RenishawProbe := activeTool;
        ENDIF
    ENDPROC
    
    !Tool Calibration using tool setter
    PROC pToolCalibrationReni()
        VAR bool boolToolCalib;
        
        activeWobj := wobj_rotBP;
        
        activeTool := tool_RenishawProbe;
        
        boolToolCalib := RenishawTCPset(activeTool, wobj_renishawToolSetter170Ruby, 40, 40, true, -10);
        
        IF boolToolCalib = FALSE THEN
            TPWrite "Tool Calibration could not be performed";
        ELSE
            TPWrite "Tool Calibration Successful";
        ENDIF
    ENDPROC
    
    
    !==================================FUNCTIONS================================!
    
    !----------- BUILDPLATE CALIBRATION ----------!
    
    !------Function for Build Plate Calibration using Touch Probe-------------!
    FUNC bool fStaticWorkObjCalib(inout wobjdata calibStaticWobj, robtarget center, num Diameter)
        VAR pose frame;
        
        VAR num offset_x;
        VAR num offset_y;
        VAR num offset_z;
        
        VAR string MechUnitConfigName;
        
        ReadCfgData "/MOC/MECHANICAL_UNIT/M7DM1","use_activation_relay",MechUnitConfigName;
        
        IF MechUnitConfigName = "STN1" THEN
            center.extax := bpZeroBruno;
        ELSE
            center.extax := bpZero;
        ENDIF
        
        SingArea \wrist;
        ConfL \On;
        
        offset_x := Diameter/2;
        offset_y := Diameter/2;
        offset_z := 150;
        
        
        GoArmHome;
        GoBPHome;
        pActivateRenishawProbe;
        
        MoveL Offs(center,0,0,2*offset_z), v100,z0,tool_RenishawProbe;
        
        ! Origin
        MoveL Offs(center,-offset_x,-offset_y,offset_z), v100,z0,tool_RenishawProbe;
        MoveL Offs(center,-offset_x,-offset_y,offset_z/2), v100,z0,tool_RenishawProbe;
        SearchL \SStop, diRenishawProbeStatus\PosFlank, sp1, Offs(center,-offset_x,-offset_y,-offset_z/2),v5,tool_RenishawProbe;
        MoveL Offs(center,-offset_x,-offset_y,offset_z), v100,z0,tool_RenishawProbe;

        
        ! Positive x -axis
        MoveL Offs(center,offset_x,-offset_y,offset_z), v100,z0,tool_RenishawProbe;
        MoveL Offs(center,offset_x,-offset_y,offset_z/2), v100,z0,tool_RenishawProbe;
        SearchL \SStop,diRenishawProbeStatus\PosFlank, sp2, Offs(center,offset_x,-offset_y,-offset_z/2),v5,tool_RenishawProbe;
        MoveL Offs(center,offset_x,-offset_y,offset_z), v100,z0,tool_RenishawProbe;

        
        ! Positive y-axis
        MoveL Offs(center,-offset_x,offset_y,offset_z), v100,z0,tool_RenishawProbe;
        MoveL Offs(center,-offset_x,offset_y,offset_z/2), v100,z0,tool_RenishawProbe;
        SearchL \SStop, diRenishawProbeStatus\PosFlank, sp3, Offs(center,-offset_x,offset_y,-offset_z/2),v5,tool_RenishawProbe;
        MoveL Offs(center,-offset_x,offset_y,offset_z), v100,z0,tool_RenishawProbe;

        
        ! Z height at the center
        MoveL Offs(center,0,0,offset_z), v100,z0,tool_RenishawProbe;
        MoveL Offs(center,0,0,offset_z/2), v100,z0,tool_RenishawProbe;
        SearchL \SStop, diRenishawProbeStatus\PosFlank, sp4, Offs(center,0,0,-offset_z/2),v5,tool_RenishawProbe;
        MoveL Offs(center,0,0,offset_z), v100,z0,tool_RenishawProbe;
        
        frame := DEFFRAME(sp1,sp2,sp3);
        !Debug
        frameDebug := frame;!FIXME
        
        frame.trans.x := center.trans.x;
        frame.trans.y := center.trans.y;
        
        calibStaticWobj.uframe := frame;
        calibStaticWobj.uframe.trans.z := sp4.trans.z - RenishawProbeRubySphereDia/2;
        
        TPWrite "Position" \Pos:=calibStaticWobj.uframe.trans;
        TPWrite "Quaternions" \Orient:=calibStaticWobj.uframe.rot;
        
        GoArmHome;
        GoBPHome;
        
        ConfL \Off;
        
        RETURN TRUE;
    
    ENDFUNC
    
    
    !------------Function for Rotating Buildplate Calibration using touch probe---------!
       FUNC bool fRotWorkObjectCalibSphere(inout wobjdata rotWobj_calib, num buildplateDiameter)

        VAR num margin := 30; !Approach distance from sphere 
        VAR num N;
        
        VAR num max_err := 0;
        VAR num mean_err := 0;
        VAR orient tiltQuat;
        VAR orient axisQuat;
        VAR orient diffQuat;
        VAR pos tiltEuler;
        VAR pos axisEuler;
        VAR pos diffEuler;
        VAR bool convert;
        VAR bool boolstaticCalib;
        VAR bool timeout;
        VAR num sphereRadius;
        VAR num approachRadius;
        
        VAR string MechUnitConfigName;
        
        ReadCfgData "/MOC/MECHANICAL_UNIT/M7DM1","use_activation_relay",MechUnitConfigName;
        
        IF MechUnitConfigName = "STN1" THEN
            center.extax := bpZeroBruno;
            rotPoint1.extax := bpZeroBruno;
        ELSE
            center.extax := bpZero;
            rotPoint1.extax := bpZero;
        ENDIF
        
        SingArea \LockAxis4;
        ConfL \On;
        GoArmHome;
        GoBPHome;
        
        
        
        pActivateRenishawProbe;
        
        IF timeout = TRUE THEN
            BookErrNo ERR_RENISHAW;
            ErrRaise "ERR_RENISHAW", renishawErrId, renishawErrTitle, ERRSTR_TASK, renishawErrStr, ERRSTR_CONTEXT,ERRSTR_EMPTY;
        ENDIF
        
        sphereRadius := MasterSphereDia/2;
        
        !approach distance from center of the sphere
        approachRadius := MasterSphereDia/2 + margin;
        
        
        !size of array or in other words, number of sectors
        N := 8;
        
        !compute and create rotation points for each sector rotation of sphere
        CreateSpherePoints center, rotPoint1, N;
        
        
        !Iterate for each sector rotation
        FOR i FROM 1 TO N DO
            
            IF rotPoints{i}.trans.z = 0 THEN
                TPWrite "points not taught or wrong points taught";
                EXIT;
            ENDIF
        
            !Robot approach movement around sphere
            MovesAroundSphere rotPoints{i},approachRadius;
            
            !create centerpoints representing exact center of sphere for each sector rotation 
            centerPoints{i} := rotPoints{i};
            !centerPoints{i}.trans := fSpherecenter(sp1.trans,sp2.trans,sp3.trans, sp4.trans);
            
            sp1dnum.x := numtodnum(sp1.trans.x);
            sp1dnum.y := numtodnum(sp1.trans.y);
            sp1dnum.z := numtodnum(sp1.trans.z);
            sp2dnum.x := numtodnum(sp2.trans.x);
            sp2dnum.y := numtodnum(sp2.trans.y);
            sp2dnum.z := numtodnum(sp2.trans.z);
            sp3dnum.x := numtodnum(sp3.trans.x);
            sp3dnum.y := numtodnum(sp3.trans.y);
            sp3dnum.z := numtodnum(sp3.trans.z);
            sp4dnum.x := numtodnum(sp4.trans.x);
            sp4dnum.y := numtodnum(sp4.trans.y);
            sp4dnum.z := numtodnum(sp4.trans.z);
            
            
            centerpointDnum{i} := fSpherecenter(sp1dnum, sp2dnum, sp3dnum, sp4dnum);
                        
            centerPoints{i}.trans.x := DnumToNum(centerpointDnum{i}.x);
            centerPoints{i}.trans.y := DnumToNum(centerpointDnum{i}.y);
            centerPoints{i}.trans.z := DnumToNum(centerpointDnum{i}.z);
           
        ENDFOR
        
        !Rotation Frame of Reference
        rotFrame:=CalcRotAxisFrame(M7DM1, centerPoints, N, max_err, mean_err);

                
        !Define center for static workobject calibration from rotational calibration
        center.trans.x := rotFrame.trans.x;
        center.trans.y := rotFrame.trans.y;
        
        
        IF rotPoints{1}.trans.z < minimumBPZ THEN
            EXIT;
        ENDIF
        
        IF center.trans.z < minimumBPZ THEN 
            TPWrite "Invalid Z for Center"; !FIXME add error handler
            EXIT;
        ENDIF
            
        
        ! Calibrate the workobject as a static workobject
        pStaticWorkObjectCalibration;
        
        
        !assign rotFrame to calibWobj
        calibWobj.uframe := rotFrame;
        
        
        !Calculate oframe
        !Tilt of the buildplate when static at angle = 0
        tiltQuat := wobj_staticBP.uframe.rot;
        tiltEuler := fQuatToEuler(tiltQuat);
        
        !Axis of Rotation
        axisQuat := rotFrame.rot;
        axisEuler := fQuatToEuler(tiltQuat);
        
        diffEuler := tiltEuler - axisEuler;
        
        IF (Abs(diffEuler.x))>0.1 OR (Abs(diffEuler.y))>0.1 OR (Abs(diffEuler.z))>0.1 THEN
            calibWobj.oframe.rot := OrientZYX(diffEuler.z, diffEuler.y, diffEuler.x);
        ELSE
            calibWobj.oframe.rot := OrientZYX(0, 0, 0);
        ENDIF
        
        ! Using average z calculated from static workobject calculation
        rotFrame.trans.z := wobj_staticBP.uframe.trans.z;
        
        ! Update the system parameters
        IF (max_err < 1.0) AND (mean_err < 0.5) THEN
            calibWobj.robhold := FALSE;
            calibWobj.ufprog := FALSE;
            calibWobj.ufmec := "M7DM1";
            calibWobj.uframe.trans := [0,0,0];
            calibWobj.uframe.rot := [1,0,0,0];
            calibWobj.oframe.trans := [0,0,0];

        ENDIF
        
        
        GoArmHome;
        GoBPHome;
        
        pDeactivateRenishaw;
        
        ConfL \On;
        
        RETURN TRUE;
        
        ERROR
        
        IF ERRNO = ERR_RENISHAW THEN
            ErrLog renishawErrid, renishawErrStr,"","","","";
        ENDIF
    ENDFUNC
    
    FUNC bool fRotWorkObjectVerification()
        VAR robtarget currentRobTarget;
        CONST robtarget zero := robTargetEmptyDown;
        CONST num epsilonHomePos := 10;
        CONST num epsilonZeroPos := 1;
        CONST speeddata processSpeed := v100;
        CONST speeddata approachSpeed := v5;
        
        SingArea \LockAxis4;
        ConfL \On;
        GoArmHome;
        GoBPHome;
        
        currentRobTarget := CRobT(\Tool:=tool_RenishawProbe \Wobj:=wobj_rotBP);
        
        IF Distance(currentRobTarget.trans, verifyRotCalibPos.trans) < epsilonHomePos THEN
            TPWrite "SUCCESS: Rot calibration in range";
            
            pActivateRenishawProbe;
            
            MoveL Offs(zero,0,4,100), processSpeed,z0,tool_RenishawProbe, \WObj:=wobj_rotBP;
            WaitTime 2; !FIXME
            SearchL \SStop, diRenishawProbeStatus\PosFlank, sp1, Offs(zero,0,0,-10),approachSpeed,tool_RenishawProbe, \WObj:=wobj_rotBP;
            MoveL Offs(zero,0,0,100), processSpeed,z0,tool_RenishawProbe, \WObj:=wobj_rotBP;
            
            IF Distance(sp1.trans, zero.trans) < epsilonZeroPos THEN
                TPWrite "SUCCESS: (0,0,0) pos on RotBP verified";
                RETURN TRUE;
            ELSE
                TPWrite "FAILED: (0,0,0) pos on RotBP NOT verified";
                TPWrite "Distance: "\Num:= Distance(sp1.trans, zero.trans);
                RETURN FALSE;
            ENDIF
                
        ELSE
            TPWrite "FAILED: Rot calibration NOT in range";
            RETURN FALSE;
        ENDIF
        
        
    ENDFUNC
    
    !-----------Helper procedure to write to controller-----------------------!
    PROC writeCfgToController(pose rotFrame)
        WriteCfgData "/MOC/SINGLE/"+BPMechUnitName, "base_frame_pos_x", rotFrame.trans.x/1000;
        WriteCfgData "/MOC/SINGLE/"+BPMechUnitName, "base_frame_pos_y", rotFrame.trans.y/1000;
        WriteCfgData "/MOC/SINGLE/"+BPMechUnitName, "base_frame_pos_z", rotFrame.trans.z/1000;
        WriteCfgData "/MOC/SINGLE/"+BPMechUnitName, "base_frame_orient_u0", rotFrame.rot.q1;
        WriteCfgData "/MOC/SINGLE/"+BPMechUnitName, "base_frame_orient_u1", rotFrame.rot.q2;
        WriteCfgData "/MOC/SINGLE/"+BPMechUnitName, "base_frame_orient_u2", rotFrame.rot.q3;
        WriteCfgData "/MOC/SINGLE/"+BPMechUnitName, "base_frame_orient_u3", rotFrame.rot.q4;
        TPReadFK reg1,"Warmstart required for calibration to take effect.", stEmpty, stEmpty, stEmpty, stEmpty, "OK";
        WarmStart;
    ENDPROC
    
    
    !---------Helper Procedure to Create points to locate sphere when the BP rotates-------!
    PROC CreateSpherePoints(robtarget center, robtarget rotPoint1, num N)
       
        VAR pos vect;
        VAR num offset;
        VAR num radius;
        VAR num theta;
        VAR string MechUnitConfigName;
        
        ReadCfgData "/MOC/MECHANICAL_UNIT/M7DM1","use_activation_relay",MechUnitConfigName;
         
         IF (N<4) THEN
             TPWrite "Cannot calibrate with less than four points";
         ENDIF
         
         vect := rotPoint1.trans - center.trans;
         
         offset := ATan2(vect.y, vect.x);
         
         radius := VectMagn([vect.x , vect.y, 0]);
         
     
        
        IF MechUnitConfigName = "STN1" THEN
            center.extax := bpZeroBruno;
            rotPoint1.extax := bpZeroBruno;
        ELSE
            center.extax := bpZero;
            rotPoint1.extax := bpZero;
        ENDIF
         
        FOR i FROM 1 TO N DO
            theta := (i-1) * (360/N);
            rotPoints{i} := rotPoint1;
            rotPoints{i}.extax := rotPoint1.extax;
            rotPoints{i}.extax.eax_a := theta;
            IF MechUnitConfigName = "STN1" THEN
            rotPoints{i}.extax.eax_a := 9E9;
            rotPoints{i}.extax.eax_b := 0;
            rotPoints{i}.extax.eax_c := theta;
            ELSE
            rotPoints{i}.extax.eax_a := theta;
            rotPoints{i}.extax.eax_b := 9E9;
            rotPoints{i}.extax.eax_c := 9E9;
            ENDIF
             
             rotPoints{i}.rot := [0,0,-1,0];
             rotPoints{i}.trans.x := center.trans.x + radius * cos(theta+offset);
             rotPoints{i}.trans.y := center.trans.y + radius * sin(theta+offset);
             rotPoints{i}.trans.z := rotPoint1.trans.z - MasterSphereDia/2;
             rotPoints{i}.robconf := PrintConfData;
         ENDFOR
    ENDPROC
    
    !-------Helper Procedure to move the touch probe approximately around the sphere to find it--------!
    PROC MovesAroundSphere(robtarget point, num approachRadius) !FIXME pass tool info through an argument or use single tool name

        VAR pos circumcenter;
        VAR num circumradius;
        VAR num computedRadius;
        VAR robtarget modPoint;
        VAR pos radialVector;
        VAR pos oldPoint;
        VAR num epsilon := 0.05;
        SingArea \Wrist;
        ConfL \On;
        
            MoveL offs(point,0,0,approachRadius),v200,fine,tool_RenishawProbe\WObj:=wobj0;!FIXME
            
            ! Approaches point in the negative X direction
            MoveL Offs(point,approachRadius,0,approachRadius),v100,fine,tool_RenishawProbe\WObj:=wobj0;
            MoveL Offs(point,approachRadius,0,0),v100,fine,tool_RenishawProbe\WObj:=wobj0; 
            SearchL \SStop, diRenishawProbeStatus\PosFlank, sp1, point,v10,tool_RenishawProbe\WObj:=wobj0; !sp1
            MoveL Offs(point,approachRadius,0,0),v100,fine,tool_RenishawProbe\WObj:=wobj0;
            MoveL Offs(point,approachRadius,0,approachRadius),v100,fine,tool_RenishawProbe\WObj:=wobj0;
            MoveL offs(point,0,0,approachRadius),v100,fine,tool_RenishawProbe\WObj:=wobj0;
            
            ! Approaches point in the positive X direction
            MoveL Offs(point,-approachRadius,0,approachRadius),v100,fine,tool_RenishawProbe\WObj:=wobj0;
            MoveL Offs(point,-approachRadius,0,0),v100,fine,tool_RenishawProbe\WObj:=wobj0;
            SearchL \SStop, diRenishawProbeStatus\PosFlank, sp2, point,v10,tool_RenishawProbe\WObj:=wobj0; !sp2
            MoveL Offs(point,-approachRadius,0,0),v100,fine,tool_RenishawProbe\WObj:=wobj0;
            MoveL Offs(point,-approachRadius,0,approachRadius),v100,fine,tool_RenishawProbe\WObj:=wobj0;
            MoveL offs(point,0,0,approachRadius),v100,fine,tool_RenishawProbe\WObj:=wobj0;
            
            ! Approaches point in the negative Y direction
            MoveL Offs(point,0,approachRadius,approachRadius),v100,fine,tool_RenishawProbe\WObj:=wobj0;
            MoveL Offs(point,0,approachRadius,0),v100,fine,tool_RenishawProbe\WObj:=wobj0;
            SearchL \SStop, diRenishawProbeStatus\PosFlank, sp3, point,v10,tool_RenishawProbe\WObj:=wobj0; !sp3
            MoveL Offs(point,0,approachRadius,0),v100,fine,tool_RenishawProbe\WObj:=wobj0;
            MoveL Offs(point,0,approachRadius,approachRadius),v100,fine,tool_RenishawProbe\WObj:=wobj0;
            
!            TPWrite "SP1" \Pos:=sp1.trans;
!            TPWrite "SP2" \Pos:=sp2.trans;
!            TPWrite "SP3" \Pos:=sp3.trans;
            
            ! Approaches point in the negative Z direction
            circleData := fCircumCenterRadius(sp1.trans,sp2.trans, sp3.trans);
            circumcenter := circleData.position;
            circumradius := circleData.number;
            
            modPoint := point;
            modPoint.trans.x := circumcenter.x;
            modPoint.trans.y := circumcenter.y;
            MoveL offs(modPoint,0,0,approachRadius),v100,fine,tool_RenishawProbe\WObj:=wobj0;
            SearchL \SStop, diRenishawProbeStatus\PosFlank, sp4, modPoint,v5,tool_RenishawProbe\WObj:=wobj0; !sp4
            MoveL offs(modPoint,0,0,approachRadius),v100,fine,tool_RenishawProbe\WObj:=wobj0;
            
            computedRadius := (sp4.trans.z - circumcenter.z) - RenishawProbeRubySphereDia/2;
            radialvector := (circumcenter - sp4.trans);
            radialVector := radialVector * (1/VectMagn(radialVector));
           
            oldPoint := point.trans;
            IF ((MasterSphereDia/2 - computedRadius) > 2*epsilon) THEN
                !Northern Hemisphere
                TPWrite "North"; !FIXME
                point.trans := sp4.trans + radialVector * ((MasterSphereDia/2) + (RenishawProbeRubySphereDia/2));
                IF Distance(oldPoint, point.trans) > MasterSphereDia/2 THEN
                    TPWrite "Bullshit calculation";
                    EXIT;
                ENDIF
                WaitTime 2;!FIXME
                MovesAroundSphere point, approachRadius;
                
            ELSEIF ((MasterSphereDia/2 - computedRadius) < -epsilon) THEN
                !Southern Hemisphere
                TPWrite "South"; !FIXME
                point.trans := sp4.trans + radialVector * ((MasterSphereDia/2) + (RenishawProbeRubySphereDia/2));
                IF Distance(oldPoint, point.trans) > MasterSphereDia/2 THEN
                    TPWrite "Bullshit calculation";
                    EXIT;
                ENDIF
                WaitTime 2;!FIXME
                MovesAroundSphere point, approachRadius;
            ENDIF
            
    ENDPROC
    
    !----------Function to compute center of four points------------!
    FUNC posDnum fSphereCenter(posDnum A, posDnum B, posDnum C, posDnum D)
        
        VAR dnum rdnum;
        VAR num r;
        VAR dnum m11;
        VAR dnum m12;
        VAR dnum m13;
        VAR dnum m14;
        VAR dnum m15;
        VAR dnum aa{4,4} := [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]];
        VAR posDnum center;
!        VAR dnum centerX;
!        VAR dnum centerY;
!        VAR dnum centerZ;
        
        
        Compute A, B, C, D;
               
        ! Find minor 1, 1.
        FOR i FROM 1 to 4 DO
            aa{i, 1} := PP{i, 1};
            aa{i, 2} := PP{i, 2};
            aa{i, 3} := PP{i, 3};
            aa{i, 4} := 1;
        ENDFOR
        
        m11 := fDeterminant(aa, 4);

        ! Find minor 1, 2.
        
        FOR i FROM 1 to 4 DO
            aa{i, 1} := -(PP{i, 1} * PP{i, 1} + PP{i, 2} * PP{i, 2} + PP{i, 3} * PP{i, 3});
            aa{i, 2} := PP{i, 2};
            aa{i, 3} := PP{i, 3};
            aa{i, 4} := 1;
        ENDFOR
       
        m12 := fDeterminant(aa, 4);

        ! Find minor 1, 3.
        
        FOR i FROM 1 to 4 DO
            aa{i, 2} := -(PP{i, 1} * PP{i, 1} + PP{i, 2} * PP{i, 2} + PP{i, 3} * PP{i, 3});
            aa{i, 1} := PP{i, 1};
            aa{i, 3} := PP{i, 3};
            aa{i, 4} := 1;
        ENDFOR
       
        m13 := fDeterminant(aa, 4);

        ! Find minor 1, 4.
        
        FOR i FROM 1 to 4 DO
            aa{i, 3} := -(PP{i, 1} * PP{i, 1} + PP{i, 2} * PP{i, 2} + PP{i, 3} * PP{i, 3});
            aa{i, 1} := PP{i, 1};
            aa{i, 2} := PP{i, 2};
            aa{i, 4} := 1;
        ENDFOR
      
        m14 := fDeterminant(aa, 4);

        ! Find minor 1, 5.
         FOR i FROM 1 to 4 DO
            aa{i, 4} := -(PP{i, 1} * PP{i, 1} + PP{i, 2} * PP{i, 2} + PP{i, 3} * PP{i, 3});
            aa{i, 1} := PP{i, 1};
            aa{i, 2} := PP{i, 2};
            aa{i, 3} := PP{i, 3};
        ENDFOR
        
        m15 := fDeterminant(aa, 4);

        ! Calculate result.
        IF (m11 = 0) THEN
        
            center := [0, 0, 0];
            r := 0;
        
        ELSE
        
            center.x := -0.5 * (m12 / m11);
            center.y := -0.5 * (m13 / m11);
            center.z := -0.5 * (m14 / m11);
            IF (center.x * center.x + center.y * center.y + center.z * center.z - (m15 / m11)) < 0 THEN
                r := 0;
            ELSE
                rdnum := (center.x * center.x + center.y * center.y + center.z * center.z - (m15 / m11));
                r := Sqrt(DnumToNum(rdnum));
            ENDIF
            TPWrite "radius" \Num:= r;!FIXME
            
        
        ENDIF
        
        RETURN center;
    ENDFUNC
    
        !----------- Function to calculate sphere center of three point and radius ----------!
    FUNC pos fSphereCenter3PointsRadius(pos A, pos B, pos C, num radius)
        VAR pos AB;
        VAR pos BC;
        VAR pos AC;
        VAR num magn;
        VAR pos normal;
        
        VAR posAndNum circumCircle;
        VAR pos circumCenter;
        VAR pos dCircumCenter;
        VAR num dSphereCenter;
        
        VAR pos sphereCenter3;
        
        circumCircle := fCircumCenterRadius(A, B, C);
        circumCenter := circumCircle.position;
        
        ! dCircumCenter is not a pos, but a 3 vector to store the distances from each of the three sample points to the center of the found circle
        dCircumCenter.x := Distance(A,circumCenter) - RenishawProbeRubySphereDia/2;
        dCircumCenter.y := Distance(B,circumCenter) - RenishawProbeRubySphereDia/2;
        dCircumCenter.z := Distance(C,circumCenter) - RenishawProbeRubySphereDia/2;
        
        AB := B - A;
        BC := C - B;
        AC := C - A;

        normal := AB * BC;
        magn := VectMagn(normal);
        normal.x := normal.x / magn;
        normal.y := normal.y / magn;
        normal.z := normal.z / magn;
        
        dSphereCenter := sqrt(pow(radius, 2) - pow(dCircumCenter.x,2));
        
        sphereCenter3 := circumCenter + dSphereCenter * normal;
        
        RETURN sphereCenter3;
        
    ENDFUNC
    
    !--------Helper Procudure to create matric from four points-------!
    PROC Compute(posDnum a, posDnum b, posDnum c, posDnum d)
    
        PP{1, 1} := a.x;
        PP{1, 2} := a.y;
        PP{1, 3} := a.z;
        PP{2, 1} := b.x;
        PP{2, 2} := b.y;
        PP{2, 3} := b.z;
        PP{3, 1} := c.x;
        PP{3, 2} := c.y;
        PP{3, 3} := c.z;
        PP{4, 1} := d.x;
        PP{4, 2} := d.y;
        PP{4, 3} := d.z;
        
    ENDPROC
  
    !-------Helper function to compute determinant-----------------!
    FUNC dnum fDeterminant(dnum aa{*,*}, num n)
    
        VAR dnum i;
        VAR dnum j;
        VAR dnum j1;
        VAr num j2;
        VAR dnum d := 0;
        VAR dnum m{4,4} := [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]; 

        IF (n = 2) THEN
        
            ! Terminate recursion.
            d := aa{1, 1} * aa{2, 2} - aa{2, 1} * aa{1, 2};
        
        ELSE
            d := 0;
            FOR j1 FROM 1 to n DO  ! Do each column.
            
                FOR i FROM 2 TO n DO ! Create minor.
                
                    j2 := 1;
                    FOR j FROM 1 to n DO 
                    
                        IF NOT (j = j1) THEN
                            m{i - 1, j2} := aa{i, j};
                            j2 := j2 + 1;
                        ENDIF
                        
                    ENDFOR
                
                ENDFOR
                ! Sum (+/-)cofactor * minor.
                d := d + numtodnum(POW(-1.0, j1+1)) * aa{1, j1} * fDeterminant(m, n-1);
            ENDFOR
        ENDIF

        return d;
    ENDFUNC
    
    !---------Helper function to compute Euler from Quat-----------!
    FUNC pos fQuatToEuler(orient quat)
        VAR pos Euler;
        
        Euler.x := EulerZYX(\X, quat);
        Euler.y := EulerZYX(\Y, quat);
        Euler.z := EulerZYX(\Z, quat);
        RETURN Euler;
    ENDFUNC
    
      !--------------Function to calibrate rotating buildplate using hole feature and touch probe----------!
    FUNC bool fRotWorkObjectCalibHole(inout wobjdata rotWobj_calib, num buildplateDiameter)
        VAR num radius;
        VAR num depth;
        VAR num N;
        VAR pose rotFrame;
        VAR num max_err := 0;
        VAR num mean_err := 0;
        VAR orient tiltQuat;
        VAR mecunit mechUnitName;
        VAR bool convert;
        VAR bool staticCalib;
        
        VAR string MechUnitConfigName;
        
        ReadCfgData "/MOC/MECHANICAL_UNIT/M7DM1","use_activation_relay",MechUnitConfigName;
        
        SingArea \LockAxis4;
        ConfL \On;
        GoArmHome;
        GoBPHome;
        
        TeachSpherePoints;
        
        ! assign the points that were taught to the rotPointsarray
        AssignPoints;
        
        ! size of array
        N := Dim(rotPoints,1);
        
        
        FOR i FROM 1 TO N DO
            
            IF rotPoints{i}.trans.z = 0 THEN
                TPWrite "points not taught or wrong points taught";
                EXIT;
            ENDIF
            IF MechUnitConfigName = "STN1" THEN
                rotPoints{i}.extax:=[9E9,0,(360/N)*(i-1),9E9,9E9,9E9];
            ELSE
                rotPoints{i}.extax:=[(360/N)*(i-1),9E9,9E9,9E9,9E9,9E9];
            ENDIF
            rotPoints{i}.rot := [0,0,-1,0];
        
            MoveL offs(rotPoints{i},0,0,300),v200,fine,tool_TouchProbe\WObj:=wobj0;
            MoveL offs(rotPoints{i},0,0,30),v200,fine,tool_TouchProbe\WObj:=wobj0;
            MoveL rotPoints{i},v10,fine,tool_TouchProbe\WObj:=wobj0;
        
            SearchL \SStop, diTouchProbe\NegFlank, sp1, Offs(rotPoints{i},-30,0,0),v10,tool_TouchProbe\WObj:=wobj0; !sp1
            MoveL rotPoints{i},v100,fine,tool_TouchProbe\WObj:=wobj0;
            SearchL \SStop, diTouchProbe\NegFlank, sp2, Offs(rotPoints{i},30,0,0),v10,tool_TouchProbe\WObj:=wobj0; !sp2
            MoveL rotPoints{i},v100,fine,tool_TouchProbe\WObj:=wobj0;
            SearchL \SStop, diTouchProbe\NegFlank, sp3, Offs(rotPoints{i},0,30,0),v10,tool_TouchProbe\WObj:=wobj0; !sp3
            MoveL rotPoints{i},v100,fine,tool_TouchProbe\WObj:=wobj0;
            SearchL \SStop, diTouchProbe\NegFlank, sp4, Offs(rotPoints{i},0,0,-30),v10,tool_TouchProbe\WObj:=wobj0; !sp4
            MoveL rotPoints{i},v100,fine,tool_TouchProbe\WObj:=wobj0;
        
            MoveL offs(rotPoints{i},0,0,300),v200,fine,tool_TouchProbe\WObj:=wobj0;
        
            rotPoints{i}.trans := fCircleCenter(sp1.trans,sp2.trans,sp3.trans);
            rotPoints{i}.trans.z := sp4.trans.z;
        ENDFOR
        
        ! Rotation Frame of Reference
        rotFrame:=CalcRotAxisFrame(M7DM1, rotPoints, N, max_err, mean_err);
        
        
        TPWrite "rotFrame Pos = ", \Pos := rotFrame.trans;
        TPWrite "rotFrame Orient = ", \Orient := rotFrame.rot;
        
        ! Define center for static workobject calibration
        center.trans.x := rotFrame.trans.x;
        center.trans.y := rotFrame.trans.y;
        IF rotPoints{1}.trans.z = 0 THEN
            TPWrite "points were not taught or wrong points were taught";
            EXIT;
        ELSE
            center.trans.z := rotPoints{1}.trans.z;
        ENDIF
            
        
        ! Calibrate the workobject as a statis workobject
        staticCalib := fStaticWorkObjCalib(activeWobj, center, buildplateDiameter);
        
        IF staticCalib = FALSE THEN
            TPWrite "Static Calibration could not be performed";
        ELSE
            TPWrite "Static Calibration Successful";
        ENDIF
        
        
         ! Tilt of the buildplate
        tiltQuat := activeWobj.uframe.rot;
        TPWrite "tiltQuat = ", \Orient := tiltQuat;
        
        
        ! Using average z calculated from static workobject calculation
        rotFrame.trans.z := activeWobj.uframe.trans.z;
        
        TPWrite "corr rotFrame Pos = ", \Pos := rotFrame.trans;
        TPWrite "corr rotFrame Orient = ", \Orient := rotFrame.rot;
        
        ! assigning valid 
        rotWobj_calib.robhold := FALSE;
        rotWobj_calib.ufprog := FALSE;
        rotWobj_calib.ufmec := BPMechUnitName;
        rotWobj_calib.uframe.trans := [0,0,0];
        rotWobj_calib.uframe.rot := [1,0,0,0];
        rotWobj_calib.oframe.trans := [0,0,0];
        rotWobj_calib.oframe.rot := [1,0,0,0];
        
        ! Update the system parameters
        IF (max_err < 1.0) AND (mean_err < 0.5) THEN

            WriteCfgData "/MOC/SINGLE/M7DM1", "base_frame_pos_x", rotFrame.trans.x/1000;
            WriteCfgData "/MOC/SINGLE/M7DM1", "base_frame_pos_y", rotFrame.trans.y/1000;
            WriteCfgData "/MOC/SINGLE/M7DM1", "base_frame_pos_z", rotFrame.trans.z/1000;
            WriteCfgData "/MOC/SINGLE/M7DM1", "base_frame_orient_u0", rotFrame.rot.q1;
            WriteCfgData "/MOC/SINGLE/M7DM1", "base_frame_orient_u1", rotFrame.rot.q2;
            WriteCfgData "/MOC/SINGLE/M7DM1", "base_frame_orient_u2", rotFrame.rot.q3;
            WriteCfgData "/MOC/SINGLE/M7DM1", "base_frame_orient_u3", rotFrame.rot.q4;
            TPReadFK reg1,"Warmstart required for calibration to take effect.", stEmpty, stEmpty, stEmpty, stEmpty, "OK";
            WarmStart;

        ENDIF
        
        GoArmHome;
        GoBPHome;
        
        ConfL \On;
        
        RETURN TRUE;
    ENDFUNC
    
    
    !-----------Procedure to manually teach location of the hole for different buildplate angles-----------!
    PROC TeachSpherePoints()
    VAR jointtarget current;
    VAR jointtarget moveTo;
    VAR num N := 8;
    VAR num ok;
    VAR string MechUnitConfigName;
        
    ReadCfgData "/MOC/MECHANICAL_UNIT/M7DM1","use_activation_relay",MechUnitConfigName;
    
    FOR i FROM 1 TO 8 DO
        
        current := CJointT();
        moveTo := current;
        
        IF MechUnitConfigName = "STN1" THEN
            moveTo.extax.eax_c := (360/8)*(i-1);
        ELSE
            moveTo.extax.eax_a := (360/8)*(i-1);
        ENDIF
        
        MoveAbsJ moveTo, v50,z0,tool_TouchProbe;
        
        TPWrite "Please teach point no." \Num:= i;
        
        TPReadFK ok, "Teaching point Complete? Move to Next?", stEmpty,stEmpty,stEmpty,stEmpty,"OK";
    ENDFOR
    ENDPROC
    
    
    !--------Helper Procedure to assign the points to an array--------------!
    PROC AssignPoints()
        rotPoints{1} := rotPoint1;
        rotPoints{2} := rotPoint2;
        rotPoints{3} := rotPoint3;
        rotPoints{4} := rotPoint4;
        rotPoints{5} := rotPoint5;
        rotPoints{6} := rotPoint6;
        rotPoints{7} := rotPoint7;
        rotPoints{8} := rotPoint8;
    ENDPROC
    
    !----------- Function to calculate center of three point ----------!
    FUNC pos fCircleCenter(pos A, pos B, pos C, \switch zeroZ)
        
        VAR num yDelta_a;
        VAR num xDelta_a;
        VAR num yDelta_b;
        VAR num xDelta_b;
        VAR num aSlope;
        VAR num bSlope;
        VAR pos center;

        yDelta_a := B.y - A.y;
        xDelta_a := B.x - A.x;
        yDelta_b := C.y - B.y;
        xDelta_b := C.x - B.x;
    

        aSlope := yDelta_a/xDelta_a;
        bSlope := yDelta_b/xDelta_b;  
        center.x := (aSlope*bSlope*(A.y - C.y) + bSlope*(A.x + B.x)
        - aSlope*(B.x+C.x) )/(2* (bSlope-aSlope) );
        center.y := -1*(center.x - (A.x+B.x)/2)/aSlope +  (A.y+B.y)/2;
        
        IF PRESENT(zeroZ) THEN
            center.z := 0;
        ELSE
            center.z := (A.z + B.z + C.z)/3;
        ENDIF
        RETURN center;
    ENDFUNC
    
    !----------- Function to calculate center of three point Vector Based ----------!
    FUNC posAndNum fCircumCenterRadius(pos A, pos B, pos C, \switch zeroZ)
        
        VAR pos BA;
        VAR pos AC;
        VAR pos CB;
        VAR num alpha;
        VAR num beta;
        VAR num gamma;
        VAR pos localcenter;
        VAR num localradius;
        VAR posAndNum circleData;
        
        BA := A-B;
        AC := C-A;
        CB := B-C;
        
        localradius := (VectMagn(BA)*VectMagn(CB)*VectMagn(AC)) / (2 * VectMagn(BA*CB));
        
        alpha := ( pow(VectMagn( CB),2) * DotProd( BA,-AC) ) / ( 2 * pow(VectMagn(BA*CB),2) );
        beta  := ( pow(VectMagn(-AC),2) * DotProd(-BA, CB) ) / ( 2 * pow(VectMagn(BA*CB),2) );
        gamma := ( pow(VectMagn( BA),2) * DotProd( AC,-CB) ) / ( 2 * pow(VectMagn(BA*CB),2) );
        
        localcenter.x := A.x * alpha + B.x * beta + C.x * gamma;
        localcenter.y := A.y * alpha + B.y * beta + C.y * gamma;
        
        IF PRESENT(zeroZ) THEN
            localcenter.z := 0;
        ELSE
            localcenter.z := A.z * alpha + B.z * beta + C.z * gamma;
            
        ENDIF
        
        
        circleData.position := localcenter;
        circleData.number := localradius - RenishawProbeRubySphereDia/2;
        
        
        RETURN circleData;
    ENDFUNC
    
    !----------- TOOL CALIBRATION ----------!
     
    !---------Procedure to calibrate Renishaw tool Setter--------------!
    PROC pRenishawToolSetterCalibration()
        !VARS for toolsetter Z location
        VAR robtarget sp0;
        
        !VARS for toolsetter X-Y location
        VAR pos modPos;
        VAR robtarget sp1;
        VAR robtarget sp2;
        VAR robtarget sp3;
        
        !Defines movement radius of tool around toolsetter. Abs min radius = radiusToolSetter + radiusTool
        VAR num radius := 30;
        VAR num safeDistance := 50;
        VAR speeddata approachSpeed := v100;
        VAR speeddata processSpeed := v5;
        
        !if using renishaw, 
        !alias EtherCat signals with Renishaw signal names
        RenishawSignalAlias;
        
        GoArmHome;
    
        !Turn on toolsetter and wait for bootup
        SetDo doRenishawPower, 1;
        WaitTime 1;
        SetDo doRenishawToolSetter, 1;
        
        WaitDI diRenishawError, 0, \MaxTime:= 5;
        
        !Move above the toolsetter
        MoveL Offs (renishawToolSetter170Ruby,0,0,safeDistance),approachSpeed,z0,tool_RenishawCylinder;
        
        ! Approaches point in the positive X direction
        MoveL Offs(renishawToolSetter170Ruby,-safeDistance,0,safeDistance),approachSpeed,fine,tool_RenishawCylinder;
        MoveL Offs(renishawToolSetter170Ruby,-safeDistance,0,-RenishawCylinderMachinedLen/3),approachSpeed,fine,tool_RenishawCylinder;
        SearchL \SStop, diRenishawToolSetterStatus\PosFlank, sp1, Offs(renishawToolSetter170Ruby,0,0,-RenishawCylinderMachinedLen/3),processSpeed,tool_RenishawCylinder;
        MoveL Offs(renishawToolSetter170Ruby,-safeDistance,0,-RenishawCylinderMachinedLen/3),approachSpeed,fine,tool_RenishawCylinder;
        MoveL Offs(renishawToolSetter170Ruby,-safeDistance,0,safeDistance),approachSpeed,fine,tool_RenishawCylinder;
        MoveL offs(renishawToolSetter170Ruby,0,0,safeDistance),approachSpeed,fine,tool_RenishawCylinder;

        ! Approaches point in the positve Y direction
        MoveL Offs(renishawToolSetter170Ruby,0,-safeDistance,safeDistance),approachSpeed,fine,tool_RenishawCylinder;
        MoveL Offs(renishawToolSetter170Ruby,0,-safeDistance,-RenishawCylinderMachinedLen/3),approachSpeed,fine,tool_RenishawCylinder;
        SearchL \SStop, diRenishawToolSetterStatus\PosFlank, sp2, Offs(renishawToolSetter170Ruby,0,0,-RenishawCylinderMachinedLen/3),processSpeed,tool_RenishawCylinder;
        MoveL Offs(renishawToolSetter170Ruby,0,-safeDistance,-RenishawCylinderMachinedLen/3),approachSpeed,fine,tool_RenishawCylinder;
        MoveL Offs(renishawToolSetter170Ruby,0,-safeDistance,safeDistance),approachSpeed,fine,tool_RenishawCylinder;
        MoveL offs(renishawToolSetter170Ruby,0,0,safeDistance),approachSpeed,fine,tool_RenishawCylinder;
    
        ! Approaches point in the negative Y direction
        MoveL Offs(renishawToolSetter170Ruby,0,safeDistance,safeDistance),approachSpeed,fine,tool_RenishawCylinder;
        MoveL Offs(renishawToolSetter170Ruby,0,safeDistance,-RenishawCylinderMachinedLen/3),approachSpeed,fine,tool_RenishawCylinder;
        SearchL \SStop, diRenishawToolSetterStatus\PosFlank, sp3, Offs(renishawToolSetter170Ruby,0,0,-RenishawCylinderMachinedLen/3),processSpeed,tool_RenishawCylinder;
        MoveL Offs(renishawToolSetter170Ruby,0,safeDistance,-RenishawCylinderMachinedLen/3),approachSpeed,fine,tool_RenishawCylinder;
        MoveL Offs(renishawToolSetter170Ruby,0,safeDistance,safeDistance),approachSpeed,fine,tool_RenishawCylinder;
        MoveL offs(renishawToolSetter170Ruby,0,0,safeDistance),approachSpeed,fine,tool_RenishawCylinder;
        
        ! Stores toolsetter X-Y location to wobj
        modPos := fCircleCenter(sp1.trans,sp2.trans, sp3.trans);
        
        wobj_renishawToolSetter170Ruby.uframe.trans := fCircleCenter(sp1.trans,sp2.trans, sp3.trans);
        renishawToolSetter170Ruby.trans.x := modPos.x;
        renishawToolSetter170Ruby.trans.y := modPos.y;
        
        
        !Search for the toolSetter top and offset the workobject oframe to match
        MoveL offs(renishawToolSetter170Ruby,RenishawCylinderMachinedDia/4,RenishawCylinderMachinedDia/4,safeDistance),approachSpeed,fine,tool_RenishawCylinder;
        SearchL \SStop, diRenishawToolSetterStatus\PosFlank, sp0, Offs(renishawToolSetter170Ruby,RenishawCylinderMachinedDia/4,RenishawCylinderMachinedDia/4,0),processSpeed,tool_RenishawCylinder;
        wobj_renishawToolSetter170Ruby.uframe.trans.z := sp0.trans.z - RenishawProbeRubySphereDia/2;
        renishawToolSetter170Ruby.trans.z := sp0.trans.z - RenishawProbeRubySphereDia/2;
        MoveL offs(renishawToolSetter170Ruby,0,0,safeDistance),approachSpeed,fine,tool_RenishawCylinder;
        
        TPWrite "wobj Ruby pos = "\Pos:=wobj_renishawToolSetter170Ruby.uframe.trans;
        TPWrite "robtarget Ruby pos = "\Pos:=renishawToolSetter170Ruby.trans;
        
        GoArmHome;
        
        pDeactivateRenishaw;
        
    ENDPROC
    
    !---------------Renishaw Ruby Calibrtion-------------------------------!
    
    FUNC bool fRenishawRubyCalibration()
        
        VAR robtarget ringCenter := robTargetEmptyDown;
        VAR pos offset;
        VAR num safeDistance := 50;
        VAR speeddata approachSpeed := v100;
        VAR speeddata processSpeed := v5;
        
        activeTool := tool_RenishawProbe;
        activeWobj := wobj_MasterRing;
        
        GoArmHome;

        pActivateRenishawProbe;
        
        !Move above the toolsetter
        MoveL Offs (ringCenter,0,0,safeDistance),approachSpeed,z0,activeTool\WObj:=activeWobj;
        
        MoveL Offs(ringCenter,0,0,-2*RenishawProbeRubySphereDia),approachSpeed,fine,activeTool\WObj:=activeWobj;
        SearchL \SStop, diRenishawToolSetterStatus\PosFlank, sp1, Offs(ringCenter,0,500,-2*RenishawProbeRubySphereDia),processSpeed,activeTool\WObj:=activeWobj;
        MoveL Offs(ringCenter,0,0,-2*RenishawProbeRubySphereDia),approachSpeed,fine,activeTool\WObj:=activeWobj;
        SearchL \SStop, diRenishawToolSetterStatus\PosFlank, sp2, Offs(ringCenter,0,-500,-2*RenishawProbeRubySphereDia),processSpeed,activeTool\WObj:=activeWobj;
        MoveL Offs(ringCenter,0,0,-2*RenishawProbeRubySphereDia),approachSpeed,fine,activeTool\WObj:=activeWobj;
        SearchL \SStop, diRenishawToolSetterStatus\PosFlank, sp3, Offs(ringCenter,-500,0,-2*RenishawProbeRubySphereDia),processSpeed,activeTool\WObj:=activeWobj;
        MoveL Offs(ringCenter,0,0,-2*RenishawProbeRubySphereDia),approachSpeed,fine,activeTool\WObj:=activeWobj;
        
        offset := fCircleCenter(sp1.trans, sp2.trans,sp3.trans,\zeroZ);
        
        activeTool.tframe.trans.x := activeTool.tframe.trans.x + offset.x;
        activeTool.tframe.trans.y := activeTool.tframe.trans.y + offset.y;
        
        sp1.trans.z := 0;
        
        MoveL Offs (ringCenter,0,0,safeDistance),approachSpeed,z0,activeTool\WObj:=activeWobj;
        
        MoveL Offs (ringCenter,Distance(ringcenter.trans, sp1.trans)+2*RenishawProbeRubySphereDia,0,safeDistance),approachSpeed,z0,activeTool\WObj:=activeWobj;
        SearchL \SStop, diRenishawToolSetterStatus\PosFlank, sp1, Offs (ringCenter,Distance(ringcenter.trans, sp1.trans)+2*RenishawProbeRubySphereDia,0,-2*RenishawProbeRubySphereDia),processSpeed,activeTool\WObj:=activeWobj;
        MoveL Offs (ringCenter,Distance(ringcenter.trans, sp1.trans)+2*RenishawProbeRubySphereDia,0,safeDistance),approachSpeed,z0,activeTool\WObj:=activeWobj;
        
        MoveL Offs (ringCenter,0,0,safeDistance),approachSpeed,z0,activeTool\WObj:=activeWobj;
                
        GoArmHome;
        
        pDeactivateRenishaw;
        
        offset.z := sp1.trans.z;
        activeTool.tframe.trans.z := activeTool.tframe.trans.z + offset.z;
        activeTool.tframe.trans.z := activeTool.tframe.trans.z - RenishawProbeRubySphereDia/2;
        
        tool_RenishawProbe := activeTool;
        
        RETURN TRUE;
        
    ENDFUNC  
    
    !-----------Activate Tool Setter--------------!
    PROC pActivateRenishawToolSetter()
    !if using renishaw, 
        !alias EtherCat signals with Renishaw signal names
        RenishawSignalAlias;
        
       
    
        !Turn on toolsetter and wait for bootup
        SetDo doRenishawPower, 1;
        WaitTime 1;
        SetDo doRenishawToolSetter, 1;
        
        WaitDI diRenishawError, 0, \MaxTime:= 5;
    ENDPROC
    
    !-----------Activate Probe--------------!
    PROC pActivateRenishawProbe()
    !if using renishaw, 
        !alias EtherCat signals with Renishaw signal names
        RenishawSignalAlias;
        
       
    
        !Turn on Probe and wait for bootup
        SetDo doRenishawPower, 1;
        WaitTime 1;
        SetDo doRenishawProbe, 1;
        
        WaitDI diRenishawError, 0, \MaxTime:= 5;
    ENDPROC
    
    !--------Turn off Renishaw--------------!
    PROC pDeactivateRenishaw()
        !alias EtherCat signals with Renishaw signal names
        RenishawSignalAlias;
        SetDo doRenishawToolSetter, 0;
        WaitTime 1;
        SetDo doRenishawProbe, 0;
        WaitTime 1;
        SetDo doRenishawPower, 0;  
    ENDPROC  
    
    !--------------RobTarget Creator--------------!
    FUNC robtarget robtargetCreate(pos position, orient quat, confdata robconfig, extjoint extaxis)
        VAR robtarget returnTarget;
        returnTarget.trans := position;
        returnTarget.rot := quat;
        returnTarget.robconf := robconfig;
        returnTarget.extax := extaxis;
        RETURN returnTarget;
    ENDFUNC
    
    FUNC robtarget robTargetEmpty()
        RETURN [[0,0,0],[1,0,0,0],[0,0,0,0],[9E9,9E9,9E9,9E9,9E9,9E9]];
    ENDFUNC
    
    
    !-----------Function to calibrate TCP using Renishaw--------------------------!
    FUNC bool RenishawTCPset(tooldata tool, wobjdata wobj, num radius, num toolDistTCPtoSidewall, bool orientSet, num orientSetZ)
        CONST robtarget renishawHomeAbove := [[0,0,200],[0,0,-1,0],[0,0,0,0],[0,9E+09,9E+09,9E+09,9E+09,9E+09]];
        CONST robtarget renishawHome := [[0,0,10],[0,0,-1,0],[0,0,0,0],[0,9E+09,9E+09,9E+09,9E+09,9E+09]];
        CONST robtarget renishawToolSetterTop := [[0,0,-1],[0,0,-1,0],[0,0,0,0],[0,9E+09,9E+09,9E+09,9E+09,9E+09]];
        VAR robtarget renishawToolSetterBottom := [[0,0,0],[0,0,-1,0],[0,0,0,0],[0,9E+09,9E+09,9E+09,9E+09,9E+09]];
        
        !VARS for toolsetter height
        VAR robtarget sp;
        VAR num zOffset;
        
        !VARS for toolsetter x-y
        VAR pos toolSetterCenterA;
        VAR jointtarget jointCenterA;
        VAR robtarget robCenterA := [[0,0,0],[0,0,-1,0],[0,0,0,0],[0,9E+09,9E+09,9E+09,9E+09,9E+09]];
        VAR robtarget centerCalcA;
        VAR robtarget centerCalcB;
        VAR robtarget centerCalcC;
        
        !Second Set is used to probe slightly higher on the tool to check for verticality
        VAR pos toolSetterCenterB;
        VAR jointtarget jointCenterB;
        VAR robtarget robCenterB := [[0,0,0],[0,0,-1,0],[0,0,0,0],[0,9E+09,9E+09,9E+09,9E+09,9E+09]];
        VAR robtarget centerCalcD;
        VAR robtarget centerCalcE;
        VAR robtarget centerCalcF;
        
        VAR pos toolSetterCenterDelta;
        VAR num toolSetterCenterDeltaMagn;
        VAR pose frameToolSetter;
        VAR orient frameTool;
        VAR pos destinationVector := [0,0,-1];
        
        !VARS for transforming tool TCP to tool0 coordinates for tool height and x-y center
        VAR pose toolPose;
        VAR pos toolPos;
        VAR pos tool0Center;
        
        VAR num Eux;
        VAR num Euy;
        VAR num Euz;
        VAR pos Euxyz;
        
        activeTool := tool;
        activeWobj := wobj;
        renishawToolSetterBottom.trans.z := -toolDistTCPtoSidewall;
        
        !Debug
        !activeTool.tframe.rot := OrientZYX(0,0,5);
        
        toolPose.trans := activeTool.tframe.trans;
        toolPose.rot := activeTool.tframe.rot;
        
        GoArmHome;
        
        !Turn on toolsetter and wait for bootup
        SetDo doRenishawPower, 1;
        WaitTime 1;
        SetDo doRenishawToolSetter, 1;
        
        WaitDI diRenishawError, 0, \MaxTime:= 5;
        
        !Move above the toolsetter
        MoveJ renishawHomeAbove, v100, z0, activeTool, \WObj:=activeWobj;
        MoveL renishawHome, v100, z0, activeTool, \WObj:=activeWobj;
        
        !Search for the toolSetter top and offset the workobject oframe to match
        SearchL\Stop, diRenishawToolSetterStatus, sp, renishawToolSetterTop, v5, activeTool \WObj:= activeWobj;
        MoveL renishawHome, v100, z0, activeTool \WObj:= activeWobj;
        
        toolPos.z := sp.trans.z;
        
        !Return above toolSetter
        MoveL renishawHomeAbove, v100, z0, activeTool, \WObj:=activeWobj;
       
        !Move next to toolsetter
        MoveL Offs (CRobT(\Tool:=activeTool \WObj:=activeWobj), -radius, 0, 0), v100, z0, activeTool, \WObj:= activeWobj;
        
        !Touch point 1 in x-y plane to determine center
        MoveL Offs (renishawToolSetterBottom, -radius, -radius/4, 0), v100, z0, activeTool, \WObj:= activeWobj;
        SearchL \Stop, diRenishawToolSetterStatus, centerCalcA, renishawToolSetterBottom, v5, activeTool, \WObj:= activeWobj;
        MoveL Offs (renishawToolSetterBottom, -radius, -radius/4, 0), v100, z0, activeTool, \WObj:= activeWobj;
        
        IF orientSet THEN
            MoveL Offs (renishawToolSetterBottom, -radius, -radius/4, orientSetZ), v100, z0, activeTool, \WObj:= activeWobj;
            SearchL \Stop, diRenishawToolSetterStatus, centerCalcD, Offs (renishawToolSetterBottom, 0, 0, orientSetZ), v5, activeTool, \WObj:= activeWobj;
            MoveL Offs (renishawToolSetterBottom, -radius, -radius/4, orientSetZ), v100, z0, activeTool, \WObj:= activeWobj;      
        ENDIF

        !Touch point 2 in x-y plane to determine center
        MoveL Offs(renishawToolSetterBottom, 0, -radius, 0), v100, z0, activeTool, \WObj:= activeWobj;
        SearchL \Stop, diRenishawToolSetterStatus, centerCalcB, renishawToolSetterBottom, v5, activeTool, \WObj:= activeWobj;
        MoveL Offs (renishawToolSetterBottom, 0, -radius, 0), v100, z0, activeTool, \WObj:= activeWobj;
        
        IF orientSet THEN
            MoveL Offs (renishawToolSetterBottom, 0, -radius, orientSetZ), v100, z0, activeTool, \WObj:= activeWobj;
            SearchL \Stop, diRenishawToolSetterStatus, centerCalcE, Offs (renishawToolSetterBottom, 0, 0, orientSetZ), v5, activeTool, \WObj:= activeWobj;
            MoveL Offs (renishawToolSetterBottom, 0, -radius, orientSetZ), v100, z0, activeTool, \WObj:= activeWobj;
        ENDIF
            
        !Touch point 3 in x-y plane to determine center
        MoveL Offs(renishawToolSetterBottom, radius, 0, 0), v100, z0, activeTool, \WObj:= activeWobj;
        SearchL \Stop, diRenishawToolSetterStatus, centerCalcC, renishawToolSetterBottom, v5, activeTool, \WObj:= activeWobj;
        MoveL Offs (renishawToolSetterBottom, radius, 0, 0), v100, z0, activeTool, \WObj:= activeWobj;
        
        IF orientSet THEN
            MoveL Offs (renishawToolSetterBottom, radius, 0, orientSetZ), v100, z0, activeTool, \WObj:= activeWobj;
            SearchL \Stop, diRenishawToolSetterStatus, centerCalcF, Offs (renishawToolSetterBottom, 0, 0, orientSetZ), v5, activeTool, \WObj:= activeWobj;
            MoveL Offs (renishawToolSetterBottom, radius, 0, orientSetZ), v100, z0, activeTool, \WObj:= activeWobj;
        ENDIF
            
        !Retract
        MoveL Offs (CRobT(\Tool:=activeTool \WObj:=activeWobj), 0, 0, 300), v100, z0, activeTool, \WObj:= activeWobj;
        
        
        
        !Calculate center points
        toolSetterCenterA := fCircleCenter(centerCalcA.trans, centerCalcB.trans, centerCalcC.trans);
        robCenterA.trans := toolSetterCenterA;
        jointCenterA := CalcJointT(robCenterA, activeTool, \Wobj:=activeWobj);
        !TPWrite "Center point A = "\Pos:=toolSetterCenterA;
        
        IF orientSet THEN
            toolSetterCenterB := fCircleCenter(centerCalcD.trans, centerCalcE.trans, centerCalcF.trans);
            robCenterB.trans := toolSetterCenterB;
            jointCenterB := CalcJointT(robCenterB, activeTool, \Wobj:=activeWobj);
            
            !TPWrite "Center point B = "\Pos:=toolSetterCenterB;
            toolSetterCenterDelta := toolSetterCenterB - toolSetterCenterA;
            TPWrite "Cdelta = "\Pos:=toolSetterCenterDelta;
            toolSetterCenterDeltaMagn := sqrt(POW(toolSetterCenterDelta.x,2) + POW(toolSetterCenterDelta.y,2));
            TPWrite "CError :=" \Num:=toolSetterCenterDeltaMagn;
            frameTool := reorientU2V(toolSetterCenterDelta,destinationVector);
        
            Euxyz.x := EulerZYX(\X, frameTool);
            Euxyz.y := EulerZYX(\Y, frameTool);
            Euxyz.z := EulerZYX(\Z, frameTool);
            
            TPWrite "Exyz:= ", \Pos:=Euxyz;
            
            !TPWrite "Tool quat deviation =" \Orient:=frameTool;
            !TPWrite "Tool quat =" \Orient:=activeTool.tframe.rot;
            
            !frameTool := activeTool.tframe.rot * frameTool;
            !TPWrite "Tool quat corrected =" \Orient:=frameTool;
            
            !activeTool.tframe.rot := frameTool;
            
            MToolRotCalib jointCenterB, jointCenterA, activeTool;
            
            IF toolSetterCenterDeltaMagn < 0.05 THEN
                
                toolPos.x := toolSetterCenterA.x;
                toolPos.z := toolSetterCenterA.y;
                !Transform x y z offsets from tool frame to tool0 frame
                tool0Center:= PoseVect(ToolPose, toolPos);
                TPWrite "Old tooldata" \Pos:=activeTool.tframe.trans;
                activeTool.tframe.trans := tool0Center;
                TPWrite "New tooldata" \Pos:=activeTool.tframe.trans;
                !Tool will hover over toolsetter to visually verify offsets
                MoveL Offs(renishawToolSetterTop, 0,0,10),v50,z0,activeTool,\WObj:=activeWobj;
                WaitTime 5;
            ENDIF
            
            GoArmHome;
            
        ENDIF
        
 
        
!        !Transform x y z offsets from tool frame to tool0 frame
!        tool0Center:= PoseVect(fdmToolPose, toolSetterCenterA);
   
!        TPWrite "Old tooldata" \Pos:=tool_FDM03.tframe.trans;
!        tool_FDM03.tframe.trans := tool0Center;
!        TPWrite "New tooldata" \Pos:=tool_FDM03.tframe.trans;        
        
!        !Tool will hover over toolsetter to visually verify offsets
!        MoveL Offs(renishawToolSetterTop, 0,0,10),v50,z0,tool_FDM03,\WObj:=reniWobj;
!        WaitTime 5;
        
        
        SetDo doRenishawToolSetter, 0;
        WaitTime 1;
        SetDo doRenishawPower, 0;
        
        RETURN TRUE;
        
    ENDFUNC
 
    !--------------Function to calculate quat from 2 vectors-------------!
    FUNC orient reorientU2V(pos u, pos v)
        VAR pos crossProd;
        VAR pos uL;
        VAR pos vL;
        VAR orient frame;
        
        uL.x := u.x/VectMagn(u);
        uL.y := u.y/VectMagn(u);
        uL.z := u.z/VectMagn(u);
        
        vL.x := v.x/VectMagn(v);
        vL.y := v.y/VectMagn(v);
        vL.z := v.z/VectMagn(v);
        
        crossProd := uL*vL;
        
        frame.q1 := sqrt(VectMagn(uL)*VectMagn(vL))+DotProd(uL,vL);
        frame.q2 := crossProd.x;
        frame.q3 := crossProd.y;
        frame.q4 := crossProd.z;
        
        frame := normQuat(frame);
        
        RETURN frame;
    ENDFUNC
    
    
    !-----------Function to normalize a quaternion--------------!
    FUNC orient normQuat (orient quat)
        !Normalizes quaternions if the normalization error is greater then 0.1
        VAR num quatMagn;
        
        quatMagn := sqrt(Pow(quat.q1,2) + Pow(quat.q2,2) + Pow(quat.q3,2) + Pow(quat.q4,2));
        quat.q1 := quat.q1/quatMagn;
        quat.q2 := quat.q2/quatMagn;
        quat.q3 := quat.q3/quatMagn;
        quat.q4 := quat.q4/quatMagn;
        
        quat := NOrient(quat);
        
        RETURN quat;
    ENDFUNC
    
    PROC ThruBeamCalib()
        VAR robtarget sp1; 
        VAR robtarget sp2; 
        VAR robtarget sp3;
        VAR num yoffset := 100;
        VAR num xoffset := 0;

        SingArea \Wrist;
        ConfL \On;
        GoArmHome;
        MoveJ Offs(thruBeamCalibHome,0,0,280), v100, z0, tool_RenishawCylinder\WObj:=wobj0;
        MoveL thruBeamCalibHome, v100, z0, tool_RenishawCylinder\WObj:=wobj0;
        SearchL \PStop, diThruBeam\PosFlank, sp1, Offs(thruBeamCalibHome,0,yoffset,0), v10, tool_RenishawCylinder\WObj:=wobj0;
        WaitTime 0.5;
        SearchL \PStop, diThruBeam\NegFlank, sp2, Offs(thruBeamCalibHome,0,yoffset,0), v10, tool_RenishawCylinder\WObj:=wobj0;
        WaitTime 0.5;
        
        thrubeamA.x := (sp1.trans.x + sp2.trans.x)/2;
        thrubeamA.y := (sp1.trans.y + sp2.trans.y)/2;
        MoveL thruBeamCalibHome, v100, z0, tool_RenishawCylinder\WObj:=wobj0;
        
        yoffset := (thrubeamA.y - thruBeamCalibHome.trans.y);
        xoffset := (thrubeamA.x - thruBeamCalibHome.trans.x);
        
        
        MoveL Offs(thruBeamCalibHome,0,0,180), v100, z0, tool_RenishawCylinder\WObj:=wobj0;
        MoveL Offs(thruBeamCalibHome,xoffset,yoffset,180), v100, z0, tool_RenishawCylinder\WObj:=wobj0;
        MoveL Offs(thruBeamCalibHome,xoffset,yoffset,50),v100, z0, tool_RenishawCylinder\WObj:=wobj0;
        SearchL \PStop, diThruBeam\PosFlank, sp3, Offs(thruBeamCalibHome,xoffset,yoffset,-50),v10,tool_RenishawCylinder\WObj:=wobj0;
        MoveL Offs(thruBeamCalibHome,xoffset,yoffset,50),v100, z0, tool_RenishawCylinder\WObj:=wobj0;
        MoveL Offs(thruBeamCalibHome,xoffset,yoffset,180), v100, z0, tool_RenishawCylinder\WObj:=wobj0;
        MoveL Offs(thruBeamCalibHome,0,0,180), v100, z0, tool_RenishawCylinder\WObj:=wobj0;
        thrubeamA.z := sp3.trans.z;
        TPErase;
        GoArmHome;
        ConfL \Off;
        ERROR
            IF ERRNO = ERR_WHLSEARCH THEN
                TRYNEXT;
            ENDIF
    ENDPROC
    
    PROC TCPCalibration()
        CONST num fraction := 1;
        VAR robtarget setpoint;
        VAR robtarget setpoint1;
        VAR robtarget tcpcalibhomeA1;
        VAR robtarget sp1a;
        VAR robtarget sp2a;
        VAR robtarget sp3a;
        VAR robtarget sp4a;
        VAR robtarget sp1b;
        VAR robtarget sp2b;
        VAR robtarget sp3b;
        VAR robtarget sp4b;
        VAR robtarget sp5;
        VAR num yoffset := 100;
        VAR num xoffset := 0;
        VAR num zoffset := 250;
        VAR num zGap := 3;
        VAR orient correction;
        VAR pos correctVect := [0,0,-1];
        VAR robtarget offsetRobTargeta := [[0,0,0],[0,0,1,0],[0,0,0,0],[0,9E+09,9E+09,9E+09,9E+09,9E+09]];
        VAR robtarget offsetRobTargetb := [[0,0,0],[0,0,1,0],[0,0,0,0],[0,9E+09,9E+09,9E+09,9E+09,9E+09]];
        VAR robtarget offsetRobTargetc := [[0,0,0],[0,0,1,0],[0,0,0,0],[0,9E+09,9E+09,9E+09,9E+09,9E+09]];
        VAR jointtarget offsetJointTargeta;
        VAR jointtarget offsetJointTargetb;
        VAR jointtarget offsetJointTargetc;
        
        
        SingArea \Wrist;
        ConfL \On;
        GoArmHome;
        
        IF calibOrient = TRUE THEN
            tcpcalibhome1.trans := [thrubeamA.x, thrubeamA.y-70, thrubeamA.z -5];
            tcpcalibhome2.trans := [thrubeamA.x, thrubeamA.y-70, thrubeamA.z -5];
        
            MoveJ Offs(tcpcalibhome1,0,0,200), v100, z0, activeTool\WObj:=wobj0;
        
            MoveL tcpcalibhome1, v100, z0, activeTool\WObj:=wobj0;
            SearchL \PStop, diThruBeam\PosFlank, sp1a, Offs(tcpcalibhome1,0,yoffset,0), v10, activeTool\WObj:=wobj0;
            WaitTime 0.5;
            SearchL \PStop, diThruBeam\NegFlank, sp2a, Offs(tcpcalibhome1,0,yoffset,0), v10, activeTool\WObj:=wobj0;
            MoveL tcpcalibhome1, v100, z0, activeTool\WObj:=wobj0;
        
            MoveL Offs(tcpcalibhome1,0,0,-zGap), v100, z0, activeTool\WObj:=wobj0;
            SearchL \PStop, diThruBeam\PosFlank, sp1b, Offs(tcpcalibhome1,0,yoffset,-zGap), v10, activeTool\WObj:=wobj0;
            WaitTime 0.5;
            SearchL \PStop, diThruBeam\NegFlank, sp2b, Offs(tcpcalibhome1,0,yoffset,-zGap), v10, activeTool\WObj:=wobj0;
            MoveL Offs(tcpcalibhome1,0,0,-zGap), v100, z0, activeTool\WObj:=wobj0;
        
            MoveL Offs(tcpcalibhome1,0,0,200), v100, z0, activeTool\WObj:=wobj0;
            WaitTime 0.5;
        
            offseta.x := ((sp1a.trans.y+sp2a.trans.y)/2) - thrubeamA.y;
            offsetb.x := ((sp1b.trans.y+sp2b.trans.y)/2) - thrubeamA.y;
            TPWrite "offset.x = " \Num:=offseta.x;
            MoveL Offs(tcpcalibhome1,0,0,80), v100, z0, activeTool\WObj:=wobj0;
            MoveJ Offs(tcpcalibhome2,0,0,80), v100, z0, activeTool\WObj:=wobj0;
        
            MoveL tcpcalibhome2, v100, z0, activeTool\WObj:=wobj0;
            SearchL \PStop, diThruBeam\PosFlank, sp3a, Offs(tcpcalibhome2,0,yoffset,0), v10, activeTool\WObj:=wobj0;
            WaitTime 0.5;
            SearchL \PStop, diThruBeam\NegFlank, sp4a, Offs(tcpcalibhome2,0,yoffset,0), v10, activeTool\WObj:=wobj0;
            MoveL tcpcalibhome2, v100, z0, activeTool\WObj:=wobj0;
        
            MoveL Offs(tcpcalibhome2,0,0,-zGap), v100, z0, activeTool\WObj:=wobj0;
            SearchL \PStop, diThruBeam\PosFlank, sp3b, Offs(tcpcalibhome2,0,yoffset,-zGap), v10, activeTool\WObj:=wobj0;
            WaitTime 0.5;
            SearchL \PStop, diThruBeam\NegFlank, sp4b, Offs(tcpcalibhome2,0,yoffset,-zGap), v10, activeTool\WObj:=wobj0;
            MoveL Offs(tcpcalibhome2,0,0,-zGap), v100, z0, activeTool\WObj:=wobj0;
        
            MoveL Offs(tcpcalibhome2,0,0,80), v100, z0, activeTool\WObj:=wobj0;
            WaitTime 0.5;
            offseta.y :=   thrubeamA.y - ((sp3a.trans.y+sp4a.trans.y)/2);
            offsetb.y :=   thrubeamA.y - ((sp3b.trans.y+sp4b.trans.y)/2);
        
            offseta.z := (sp1a.trans.z + sp2a.trans.z + sp3a.trans.z + sp4a.trans.z)/4;
            offsetb.z := (sp1b.trans.z + sp2b.trans.z + sp3b.trans.y+sp4b.trans.y)/2;
            TPWrite "offset.y = " \Num:=offseta.y;
        
            offsetRobTargeta.trans.x := (sp3a.trans.x + sp4a.trans.x)/2;
            offsetRobTargeta.trans.y := (sp3a.trans.y + sp4a.trans.y)/2;
            offsetRobTargeta.trans.z := (sp3a.trans.z + sp4a.trans.z)/2;
            offsetRobTargetb.trans.x := (sp3b.trans.x + sp4b.trans.x)/2;
            offsetRobTargetb.trans.y := (sp3b.trans.y + sp4b.trans.y)/2;
            offsetRobTargetb.trans.z := (sp3b.trans.z + sp4b.trans.z)/2;
            offsetRobTargetc := Offs(offsetRobTargetb,5,0,0);
            offsetRobTargeta.robconf := sp3a.robconf;
            offsetRobTargeta.rot := sp3a.rot;
            offsetRobTargeta.extax := sp3a.extax;
        
        
            offsetJointTargeta := CalcJointT(offsetRobTargeta,activeTool,\Wobj:=wobj0);
            offsetJointTargetb := CalcJointT(offsetRobTargetb,activeTool,\Wobj:=wobj0);
            offsetJointTargetc := CalcJointT(offsetRobTargetc,activeTool,\Wobj:=wobj0);
        
            MToolRotCalib offsetJointTargetb, offsetJointTargeta, \XPos:=offsetJointTargetc, activeTool;
        
            TPWrite "toolframe" \Orient:=activeTool.tframe.rot;
        ENDIF
        
        MoveJ Offs(tcpcalibhome1,0,0,100), v100, z0, activeTool\WObj:=wobj0;
        
        MoveL tcpcalibhome1, v100, z0, activeTool\WObj:=wobj0;
        SearchL \PStop, diThruBeam\PosFlank, sp1a, Offs(tcpcalibhome1,0,yoffset,0), v10, activeTool\WObj:=wobj0;
        WaitTime 0.5;
        SearchL \PStop, diThruBeam\NegFlank, sp2a, Offs(tcpcalibhome1,0,yoffset,0), v10, activeTool\WObj:=wobj0;
        MoveL tcpcalibhome1, v100, z0, activeTool\WObj:=wobj0;
        
        MoveL Offs(tcpcalibhome1,0,0,150), v100, z0, activeTool\WObj:=wobj0;
        WaitTime 0.5;
        
        offseta.x := ((sp1a.trans.y+sp2a.trans.y)/2) - thrubeamA.y;
        TPWrite "offset.x = " \Num:=offseta.x;
        MoveL Offs(tcpcalibhome1,0,0,80), v100, z0, activeTool\WObj:=wobj0;
        MoveJ Offs(tcpcalibhome2,0,0,80), v100, z0, activeTool\WObj:=wobj0;
        
        MoveL tcpcalibhome2, v100, z0, activeTool\WObj:=wobj0;
        SearchL \PStop, diThruBeam\PosFlank, sp3a, Offs(tcpcalibhome2,0,yoffset,0), v10, activeTool\WObj:=wobj0;
        WaitTime 0.5;
        SearchL \PStop, diThruBeam\NegFlank, sp4a, Offs(tcpcalibhome2,0,yoffset,0), v10, activeTool\WObj:=wobj0;
        MoveL tcpcalibhome2, v100, z0, activeTool\WObj:=wobj0;
        
        MoveL Offs(tcpcalibhome2,0,0,80), v100, z0, activeTool\WObj:=wobj0;
        MoveJ Offs(tcpcalibhome1,0,0,80), v100, z0, activeTool\WObj:=wobj0;
        WaitTime 0.5;
        offseta.y := thrubeamA.y - ((sp3a.trans.y + sp4a.trans.y)/2);
        offseta.z := 0;
        
        activeTool.tframe.trans := PoseVect(activeTool.tframe, offseta);
        
        yoffset := (thrubeamA.y - tcpcalibhome1.trans.y);
        xoffset := (thrubeamA.x - tcpcalibhome1.trans.x);
        
        MoveL Offs(tcpcalibhome1,0,0,200), v100, z0, activeTool\WObj:=wobj0;
        MoveL Offs(tcpcalibhome1,xoffset,yoffset,200), v100, z0, activeTool\WObj:=wobj0;
        MoveL Offs(tcpcalibhome1,xoffset,yoffset,50),v100, z0, activeTool\WObj:=wobj0;
        SearchL \PStop, diThruBeam\PosFlank, sp5, Offs(tcpcalibhome1,xoffset,yoffset,-50),v10,activeTool\WObj:=wobj0;
        WaitTime 1;
        MoveL Offs(tcpcalibhome1,xoffset,yoffset,50),v100, z0, activeTool\WObj:=wobj0;
        MoveL Offs(tcpcalibhome1,xoffset,yoffset,200), v100, z0, activeTool\WObj:=wobj0;
        MoveL Offs(tcpcalibhome1,0,0,200), v100, z0, activeTool\WObj:=wobj0;
        GoArmHome;
        offseta.z := (sp5.trans.z - thrubeamA.z + 0.12);
        offseta.x := 0;
        offseta.y := 0;
        TPWrite "offset1.z = " \Num:=offseta.z;
        activeTool.tframe.trans := PoseVect(activeTool.tframe, offseta);
        !activeTool.tframe.trans.z := activeTool.tframe.trans.z + offset.z;
        TPWrite "offset= " \Pos:=offseta;
        !TPErase;
        
        TPWrite "Updated Tool Position" \Pos:= activeTool.tframe.trans;
        
        ConfL \Off;
        ERROR
            IF ERRNO = ERR_WHLSEARCH THEN
                TRYNEXT;
            ENDIF
        
    ENDPROC
    
    
    !------------Touch Probe Calibration-------------!
    PROC pCalibTP()
        
        SingArea \LockAxis4;
        ConfL \On;
        
        MoveL touch_point,v100,z0,tool_TouchProbe;
        MoveL Offs(touch_point,0,0,-100),v100,z0,tool_TouchProbe;
        SearchL \SStop, diTouchProbe\NegFlank, tp, Offs(touch_point,0,0,-400),v5,tool0\WObj:=wobj_staticBP;
        MoveL touch_point,v100,z0,tool_TouchProbe;
        
        ConfL \Off;
        
        tool_TouchProbe.tframe.trans.z := tp.trans.z - 10;
        
    ENDPROC
    
    !--------Touch Probe Repeatability Test in z-------!
    PROC pTestTouchProbeZ()
        VAR num i;
        SingArea \LockAxis4;
        ConfL \On;
        
        MoveL touchpointz, v200,z0,tool_TouchProbe\WObj:=wobj_staticBP;
        FOR i FROM 1 TO 10 DO
        MoveL offs(touchpointz,0,0,-80), v50,z0,tool_TouchProbe\WObj:=wobj_staticBP;
        SearchL \SStop, diTouchProbe\NegFlank, sp1, Offs(touchpointz,0,0,-400),v10,tool_TouchProbe\WObj:=wobj_staticBP;
        touchpoints{i}.z := sp1.trans.z;
        ENDFOR
        
        MoveL touchpointz, v200,z0,tool_TouchProbe\WObj:=wobj_staticBP;
        
        ConfL \Off;
    ENDPROC
    
    !--------Touch Probe Repeatability Test in x&y-----!
    PROC pTestTouchProbeXY()
        VAR num i;
        VAR robtarget sp1;
        
        SingArea \LockAxis4;
        ConfL \On;
        
        MoveL touchpointz, v200,z0,tool_TouchProbe\WObj:=wobj_staticBP;
        MoveL offs(touchpointz,0,560,0), v200,z0,tool_TouchProbe\WObj:=wobj_staticBP;
        MoveL touchpointxy, v100,z0,tool_TouchProbe\WObj:=wobj_staticBP;
        FOR i FROM 1 TO 10 DO
        MoveL Offs(touchpointxy,0,-35,0), v100,z0,tool_TouchProbe\WObj:=wobj_staticBP;
        SearchL \SStop, diTouchProbe\NegFlank, sp1, Offs(touchpointxy,0,-400,0),v10,tool_TouchProbe\WObj:=wobj_staticBP;
        touchpoints{i}.x := sp1.trans.x;
        touchpoints{i}.y := sp1.trans.y;
        ENDFOR
        MoveL touchpointxy, v100,z0,tool_TouchProbe\WObj:=wobj_staticBP;
        MoveL offs(touchpointz,0,630,0), v200,z0,tool_TouchProbe\WObj:=wobj_staticBP;
        MoveL touchpointz, v200,z0,tool_TouchProbe\WObj:=wobj_staticBP;
        
        ConfL \Off;
        
    ENDPROC
    
    !--------Build Plate Planarity sampling using touch probe-----! 
    PROC pTestBuildPlatePlanarity(num run)
        VAR num i;
        VAR robtarget sp1;
        
        SingArea \LockAxis4;
        ConfL \On;
        
        FOR i FROM 1 TO 59 DO
        touchpointz.trans := p{i};
        MoveL touchpointz, v100,z0,tool_TouchProbe\WObj:=wobj_staticBP;
        MoveL Offs(touchpointz,0,0,-85), v100,z0,tool_TouchProbe\WObj:=wobj_staticBP;
        SearchL \SStop, diTouchProbe\NegFlank, sp1, Offs(touchpointz,0,0,-120),v5,tool_TouchProbe\WObj:=wobj_staticBP;
        MoveL Offs(touchpointz,0,0,-40), v100,z0,tool_TouchProbe\WObj:=wobj_staticBP;
        IF run = 1 THEN
        buildplatepoints{i} := sp1.trans;
        ELSEIF run =2 THEN
        buildplatepoints2{i} := sp1.trans;
        ENDIF
        ENDFOR
        
        MoveL touchpointz, v100,z0,tool_TouchProbe\WObj:=wobj_staticBP;
        
        ConfL \Off;
    ENDPROC

     PROC try()!Debugging
        
        VAR posAndNum circle;
        VAR pos a := [1709.22,21.3069,484.281];
            VAR pos b := [1685.62,21.3218,484.254];
            VAR pos c:=[1697.95,29.7936,484.272];
        circle := fCircumCenterRadius(a,b,c);
        TPWrite "rad" \Num:=circle.number;
        TPWrite "center" \POs:=circle.position;
    ENDPROC
    
ENDMODULE