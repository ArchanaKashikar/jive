MODULE MainModule
    
!!!----------------------------------********---------------------------------------------!!!
! Company Confidential
! ArevoJive MainModule Version 1.0
! Generated at Arevo | 8/3/2018 | Authors - Archana Kashikar
! Purpose - This module contains procedures save Files to hard disk. To be run upon installation of program files.
!
! Changes-
!!!----------------------------------********---------------------------------------------!!!
    
    PROC Main()
       
        SaveFiles;
        
        
    ENDPROC
    
    PROC SaveFiles()
        VAR string fileNames{7} := ["Calibration","Debug","Home","PointCloud","RRI","SocketComm","ToolDefs_DED"];
        VAR string thisFileName;
        VAR num N;
        
        N := Dim(fileNames,1);
        
        FOR i FROM 1 TO N DO
            
        thisFileName := "HOME:/ArevoJive/"+fileNames{i}+".sys";
        IF IsFile(thisFileName) RemoveFile(thisFileName);
        Save fileNames{i} \FilePath:=thisFileName;
        
        ENDFOR
    ENDPROC
    

ENDMODULE