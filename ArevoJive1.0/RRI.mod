MODULE RRI
!!!----------------------------------********---------------------------------------------!!!
! Company Confidential
! ArevoJive RRI Version 1.0
! Generated at Arevo | 8/3/2018 | Authors - Archana Kashikar, Wiener Mondesir
! Purpose - This module contains procedures to open and close Robot Reference Interface which broadcasts data such as TCP location.
!
! Changes-
!!!----------------------------------********---------------------------------------------!!!
    
    RECORD robdata
     num Item1;   ! a dummy, because Rapid does not allow empty Record
    ENDRECORD
    
    RECORD sensdata
     num Item1;   ! a dummy, because Rapid does not allow empty Record
    ENDRECORD
    
     !Sensor Declarations
    PERS sensor RsMaster:=[1,0,3];
    PERS robdata DataOut:=[0];
    PERS sensdata DataIn:=[0];
    
    ! Cycle data update rate in msec
    PERS num RRI_CyclicRate := 20;
        
    VAR intnum int1;

    !// RRI related
    ! Setup Interface Procedure
    PROC RRI_Open()
        RsMaster:=[1,4,3];
        IF RsMaster.State <> STATE_CLOSED THEN
            RRI_Close;
        ENDIF
        SiConnect RsMaster \NoStop;
        ! Send and receive data cyclic with 4 ms rate
        SiSetCyclic RsMaster, DataOut, RRI_CyclicRate;
        !SiGetCyclic RsMaster, DataIn, 4;
        
        IDelete int1;
        CONNECT int1 WITH readSensor;
        
        ITimer 0.1, int1;
        
        ERROR
        ErrorNum := ERRNO;
        IF ERRNO = ERR_COMM_INIT THEN
            RRI_Close;
            RETRY;
        ENDIF
        
    ENDPROC
    
    TRAP readSensor
        DataOut.Item1 := aiDistanceLaser;
    ENDTRAP 
    
    ! Close Interface Procedure
    PROC RRI_Close()
        ! Delete the interrupt
        IDelete int1;
        
        ! Close the connection
        SiClose RsMaster;
        
        ERROR
        ErrorNum := ERRNO;
    ENDPROC
    ! End RRI related
ENDMODULE