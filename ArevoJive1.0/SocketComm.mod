MODULE SocketComm
!!!----------------------------------********---------------------------------------------!!!
! Company Confidential
! ArevoJive SocketComm Version 1.0
! Generated at Arevo | 8/3/2018 | Authors - Archana Kashikar
! Purpose - This module contains procedures to enbale socket communication with other devices on the network in 
!           a server-slient infrastructure
!
! Changes-
!!!----------------------------------********---------------------------------------------!!!
    VAR socketdev server_socket; 
    VAR socketdev client_socket; 
    VAR rawbytes recieve_data;
    VAR socketstatus state;
    VAR socketstatus clientstate;
    VAR byte incommand1:=0;
    VAR num incommand;
    VAR num incommandnum;
    VAR num argument{10};
    VAR robtarget Cpos;
    PERS tooldata Ctool;
    PERS wobjdata Cwobj;
    VAR robtarget pos_feedback;
    VAR rawbytes tcp_rawdata; 
    VAR rawbytes send_rawdata;
    VAR string string1;
    VAR confdata  conf   := [0,0,0,0];
    VAR extjoint  extj   := [9E9,9E9,9E9,9E9,9E9,9E9];
    VAR speeddata speed0 := [50.000, 30, 5000, 1000];
    VAR speeddata speed1 := [200.000, 30, 5000, 1000];
    VAR speeddata speed2 := [10.000, 30, 5000, 1000];
    VAR zonedata  zone0  := [ FALSE, 0, 15, 15, 1.5, 15, 1.5 ];
    VAR zonedata zone1 := fine;
    VAR Bool connected:=false;
    VAR intnum intno1;
    VAR triggdata trigg1;
    VAR string client_ip; 
    VAR num counter;
    VAR num recievedatalen;
    VAR orient quat;
    
    PROC MS_Initialize()
        Ctool := tool_LaserEndEffector;
        Cwobj := wobj_staticBP;
        extj.eax_a := 0;
    ENDPROC
    
    PROC SocketServer()
    Clear counter;
    
    MS_Initialize;
   
    SocketCreate server_socket;
    !SocketBind server_socket, "0.0.0.0", 2000;
    SocketBind server_socket, "192.168.1.36", 2000;
    SocketListen server_socket;
    
    
    WHILE TRUE DO
        state := SocketGetStatus(server_socket);
        clientstate:=SocketGetStatus(client_socket);
        SocketAccept server_socket, client_socket;
        TPWrite "Connected";
        connected := TRUE;
        clientstate:=SocketGetStatus(client_socket);
        state := SocketGetStatus(server_socket); 
        WHILE connected DO
            ClearRawBytes send_rawdata;
            ClearRawBytes recieve_data;
            TPWrite "==Listening==";
            SocketReceive client_socket \RawData:= recieve_data;
            recievedatalen := RawBytesLen(recieve_data);
            TPWrite "socketstatus " \Num:=RawBytesLen(recieve_data);
            IF connected THEN
            incommandnum :=0;
            UnpackRawBytes recieve_data, 1, incommandnum \IntX := DINT;
            Incr counter;
            !TPWrite "incoming command is " \Num:=incommandnum; 
            !TPWrite "command num is " \Num:=counter; 
            
            TEST incommandnum 
                
            CASE 0x0100: !Home
                
                GoArmHome;
                GoBPHome;
                ClearRawBytes send_rawdata;
                TPWrite "Home";
                packAck(incommandnum);
	       
		    CASE 0x0110:!Absolute (x,y,z,speed)
                FOR i FROM 1 TO 5 DO
                UnpackRawBytes recieve_data, 4*i + 1, argument{i} \Float4;
                ENDFOR
                Cpos := CRobT(\Tool:=Ctool,\WObj:=Cwobj);
                Cpos.trans.x := argument{1};
                Cpos.trans.y := argument{2};
                Cpos.trans.z := argument{3};
                Cpos.robconf := conf;
                !Cpos.extax := extj;
                speed0.v_tcp := argument{4};
                MoveL Cpos,speed0,zone1,Ctool\WObj:=Cwobj;
                ClearRawBytes send_rawdata;
		        PackAck(incommandnum);
		     
		    CASE 0x0120:!Relative
                FOR i FROM 1 TO 5 DO
                UnpackRawBytes recieve_data, 4*i + 1, argument{i} \Float4;
                ENDFOR
                Cpos := CRobT(\Tool:=Ctool,\WObj:=Cwobj);
                speed0.v_tcp := argument{4};
                MoveL Offs(Cpos,argument{1},argument{2},argument{3}),speed0,zone1,Ctool\WObj:=Cwobj;
                ClearRawBytes send_rawdata;
		        PackAck(incommandnum);

		    CASE 0x0130:!TCP
                Cpos := CRobT(\Tool:=Ctool,\WObj:=Cwobj);
		        SocketSend client_socket \Str := "TCP is ";
                ClearRawBytes send_rawdata;
		        PackTCP;
                
            CASE 0x0210:!Absolute Coordinated with more arguments, Euler version
                FOR i FROM 1 TO 8 DO
                UnpackRawBytes recieve_data, 4*i + 1, argument{i} \Float4;
                ENDFOR
                Cpos := CRobT(\Tool:=Ctool,\WObj:=Cwobj);
                Cpos.trans.x := argument{1};
                Cpos.trans.y := argument{2};
                
                IF argument{3} < 0 THEN
                    ClearRawBytes send_rawdata;
                    PackError(2);
                    EXIT;
                ELSE
                    Cpos.trans.z := argument{3};
                ENDIF
                
                Cpos.robconf := conf;
                Cpos.rot := OrientZYX(argument{4},argument{5},argument{6});
                Cpos.extax.eax_a := argument{7};
                speed0.v_tcp := argument{8};
                MoveL Cpos,speed0,zone1,Ctool\WObj:=Cwobj;
                ClearRawBytes send_rawdata;
		        PackAck(incommandnum);
                
            CASE 0x0310:!Absolute Coordinated with more arguments, Quaternion version 
                FOR i FROM 1 TO 9 DO
                UnpackRawBytes recieve_data, 4*i + 1, argument{i} \Float4;
                ENDFOR
                TPWrite "0x0310";
                Cpos := CRobT(\Tool:=Ctool,\WObj:=Cwobj);
                Cpos.trans.x := argument{1};
                Cpos.trans.y := argument{2};
                
                IF argument{3} < 0 THEN
                    ClearRawBytes send_rawdata;
                    PackError(2);
                    SocketSend client_socket \RawData:= send_rawdata;
                    EXIT;
                ELSE
                    Cpos.trans.z := argument{3};
                ENDIF
                
                Cpos.robconf := conf;
                Cpos.rot := [argument{4},argument{5},argument{6},argument{7}];
                Cpos.extax.eax_a := argument{8};
                speed0.v_tcp := argument{9};
                MoveL Cpos,speed0,zone1,Ctool\WObj:=Cwobj;
                ClearRawBytes send_rawdata;
		        PackAck(incommandnum);
                
            CASE 0x0320:!Absolute Uncoordinated with more arguments, Quarternion version
                FOR i FROM 1 TO 9 DO
                UnpackRawBytes recieve_data, 4*i + 1, argument{i} \Float4;
                ENDFOR
                TPWrite "0x0320";
                Cpos := CRobT(\Tool:=Ctool,\WObj:=Cwobj);
                Cpos.trans.x := argument{1};
                Cpos.trans.y := argument{2};
                
                IF argument{3} < 0 THEN
                    ClearRawBytes send_rawdata;
                    PackError(2);
                    SocketSend client_socket \RawData:= send_rawdata;
                    EXIT;
                ELSE
                    Cpos.trans.z := argument{3};
                ENDIF
                
                Cpos.robconf := conf;
                Cpos.rot := [argument{4},argument{5},argument{6},argument{7}];
                Cpos.extax.eax_a := argument{8};
                speed0.v_tcp := argument{9};
                MS_Unwind Cpos.extax, speed0,zone1,Ctool\WObj:=Cwobj;
                MoveL Cpos,speed0,zone1,Ctool\WObj:=Cwobj;
                ClearRawBytes send_rawdata;
		        PackAck(incommandnum);
            
            DEFAULT:
                ClearRawBytes send_rawdata;
                PackError(1);
            ENDTEST
            
            TPWrite "sending";
            SocketSend client_socket \RawData:= send_rawdata;
            TPWrite "ack sent";
                    !SocketSend client_socket \Str := "End of one execution";    
            ENDIF
        
        ENDWHILE
            SocketClose client_socket; 
        !ENDWHILE
    ENDWHILE
        ERROR 
            TEST ERRNO
            CASE ERR_SOCK_TIMEOUT:
                 TRYNEXT;
            CASE ERR_SOCK_CLOSED:
            connected := FALSE;
                !SocketClose client_socket;
                ClearRawBytes recieve_data;
                TRYNEXT; 
            DEFAULT:
            TPWrite "Unknown Error";
            ENDTEST
        ENDPROC

    
    PROC SocketClient()
        VAR num retry_no := 0;
        VAR string recievemsg;
        VAR socketdev my_socket;
        SocketCreate my_socket;
        SocketConnect my_socket, "192.168.1.169", 2000;
        SocketSend my_socket \Str := "Hello"; 
        SocketReceive my_socket \Str := recievemsg;
        TPWrite recievemsg;
    ERROR
     IF ERRNO = ERR_SOCK_TIMEOUT THEN
        IF retry_no < 5 THEN
           WaitTime 1;
           retry_no := retry_no + 1;
           RETRY;
    ELSE RAISE;
        ENDIF 
    ENDIF
    ENDPROC
    
    PROC PackTCP()
        VAR robtarget pos_feedback;
        
        pos_feedback := CRobT(\Tool:=Ctool,\WObj:=Cwobj);        
        TPWrite "Moving to " \Pos:= pos_feedback.trans;
        PackRawBytes pos_feedback.trans.x, send_rawdata, RawBytesLen(send_rawdata)+1 \Float4;
        PackRawBytes pos_feedback.trans.y, send_rawdata, RawBytesLen(send_rawdata)+1 \Float4;
        PackRawBytes pos_feedback.trans.z, send_rawdata, RawBytesLen(send_rawdata)+1 \Float4;
    ENDPROC
    
    PROC PackAck(num cmdId)
        PackRawBytes cmdId, send_rawdata, RawBytesLen(send_rawdata)+1 \IntX := DINT;
        PackRawBytes StrToByte("55" \Hex), send_rawdata, RawBytesLen(send_rawdata)+1 \Hex1;
        !SocketSend client_socket \RawData:= send_rawdata;
    ENDPROC
    
    PROC PackError(num errId)
        PackRawBytes errId, send_rawdata, RawBytesLen(send_rawdata)+1 \IntX := DINT;
        TEST errid
        CASE 1:
        PackRawBytes "invalid command", send_rawdata, RawBytesLen(send_rawdata)+1 \ASCII;
        CASE 2:
        PackRawBytes " z coordinate cannot be less than zero", send_rawdata, RawBytesLen(send_rawdata)+1 \ASCII;
        ENDTEST
    ENDPROC
    
    PROC MS_Unwind(extjoint extj, speeddata speed, zonedata zone, PERS tooldata tool, \PERS wobjdata wobj)
        VAR jointtarget current;
        VAR jointtarget rotate;
        current := CJointT();
        rotate.robax := current.robax;
        rotate.extax := extj;
        MoveAbsJ rotate,speed, fine, tool;
ENDPROC
ENDMODULE