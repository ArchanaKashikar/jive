MODULE ManualTCP
    !#region Copyrightę ABB Inc., Robotics Division - NA 2015
    !------------------------------------------------------------------------------
    ! All rights are reserved.  Reproduction or transmission in whole or in part,
    ! in any form or by any means, electronic, mechanical or otherwise, is 
    ! prohibited without the prior written consent of the copyright owner.
    !
    ! Filename: TCP.mod
    !------------------------------------------------------------------------------
    !#endregion
    !#region TYPE_DECL_SECTION
    !------------------------------------------------------------------------------
    !Tool Data
    TASK PERS tooldata tPointer:=[TRUE,[[43.0453,74.3182,281.106],[0.00591837,-0.00121971,-0.00976004,-0.999934]],[2.2,[0,0,0.1],[1,0,0,0],0,0,0]];
    !------------------------Joints for Tool Cal------------------------------------
    CONST jointtarget jtTipRef:=[[2.7421,17.4781,17.1984,-178.612,-55.3659,1.29964],[9E+09,0,0,9E+09,9E+09,9E+09]];
    CONST jointtarget jtTcpCal_2:=[[9.14856,23.3109,13.0469,-201.93,-64.0985,36.5304],[9E+09,0,0,9E+09,9E+09,9E+09]];
    CONST jointtarget jtTcpCal_3:=[[-3.98255,16.8487,18.4329,-155.86,-58.9167,-32.5774],[9E+09,0,0,9E+09,9E+09,9E+09]];
    CONST jointtarget jtTcpCal_4:=[[-0.120228,27.3635,4.23129,-173.897,-85.195,-30.1946],[9E+09,0,0,9E+09,9E+09,9E+09]];
    CONST jointtarget jtZelong:=[[2.74189,16.9497,16.5748,-178.631,-56.5192,1.33269],[9E+09,0,0,9E+09,9E+09,9E+09]];
    CONST jointtarget jtXelong:=[[2.65567,20.0004,13.8576,-178.627,-56.1838,1.23833],[9E+09,0,0,9E+09,9E+09,9E+09]];

    PROC ManualCalibrateTcp()
        !Calibration of TCP and rotation for moving tool
        !The tool that is to be calibrated must be mounted on the robot
        !and defined with correct component robhold (TRUE).
        !Teach each point and then run the MToolTCPCalib and MToolRotCalib function
        !!!!Do not play through this!!!!
        !
        VAR num nMaxErr;
        VAR num nMeanErr;
        !
        MoveAbsJ jtTipRef,v50,fine,tPointer;
        MoveAbsJ jtTcpCal_2,v50,fine,tPointer;
        MoveAbsJ jtTcpCal_3,v50,fine,tPointer;
        MoveAbsJ jtTcpCal_4,v50,fine,tPointer;
        MoveAbsJ jtZelong,v50,fine,tPointer;
        MoveAbsJ jtXelong,v50,fine,tPointer;
        !
        MToolTCPCalib jtTipRef,jtTcpCal_2,jtTcpCal_3,jtTcpCal_4,tPointer,nMaxErr,nMeanErr;
        TPWrite "Max error = ",\Num:=nMaxErr;
        TPWrite "Mean error = ",\Num:=nMeanErr;
        MToolRotCalib jtTipRef,jtZelong\XPos:=jtXelong,tPointer;
        Stop;
    ENDPROC

ENDMODULE