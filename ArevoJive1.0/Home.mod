MODULE Home
!!!----------------------------------********---------------------------------------------!!!
! Company Confidential
! ArevoJive Home Version 1.0
! Generated at Arevo | 8/3/2018 | Author - Archana Kashikar
! Purpose - This module contains homing procedures that are used by external interfaces in Arevo 
!           printers to home the robot and build platform
!
! Changes-
!!!----------------------------------********---------------------------------------------!!!
    
    PROC Home0()
        GoArmHome;
    ENDPROC
    
    PROC Home_buildplate()
        GoArmHome;
        GoBPHome;
    ENDPROC
ENDMODULE